﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using GandaCarsAPI.Data;
using GandaCarsAPI.Data.Repositories;
using GandaCarsAPI.Models.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NSwag;
using NSwag.SwaggerGeneration.Processors.Security;

namespace GandaCarsAPI {
    public class Startup {
        public Startup(IConfiguration configuration, IHostingEnvironment env) {
            Configuration = configuration;
            CurrentEnvironment = env;
        }

        public IConfiguration Configuration { get; }
        private IHostingEnvironment CurrentEnvironment { get; set; }

        public void ConfigureServices(IServiceCollection services) {
            services.AddCors(options => {
                options.AddPolicy("AllowAll",
                    builder => {
                        builder
                            .AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .AllowCredentials();
                    });
            });
            if (CurrentEnvironment.IsDevelopment()) {
                services.AddDbContext<ApplicationDbContext>(options => {
                    options.UseSqlServer(Configuration.GetConnectionString("GandaCarsDevelopmentContext"));
                });
            } else {
                services.AddDbContext<ApplicationDbContext>(options => {
                    //options.UseSqlServer(Configuration.GetConnectionString("GandaCarsDevelopmentContext"));
                    options.UseSqlServer(Configuration.GetConnectionString("GandaCarsProductionContext"));
                });
            }

            services.AddSession();

            services.AddOpenApiDocument(c => {
                c.DocumentName = "apidocs";
                c.Title = "Ganda Cars API";
                c.Version = "v1";
                c.Description = "The Ganda Cars API documentation description.";
                c.DocumentProcessors.Add(new SecurityDefinitionAppender("JWT Token", new SwaggerSecurityScheme {
                    Type = SwaggerSecuritySchemeType.ApiKey,
                        Name = "Authorization",
                        In = SwaggerSecurityApiKeyLocation.Header,
                        Description = "Copy 'Bearer' + valid JWT token into field"
                }));
                c.OperationProcessors.Add(new OperationSecurityScopeProcessor("JWT Token"));
            }); //for OpenAPI 3.0 else AddSwaggerDocument();

            services.AddSignalR();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2).AddJsonOptions(options => {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                //date to def
                options.SerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
                options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Unspecified;
            });;

            services.AddScoped<IBusChauffeurRepository, BusChauffeurRepository>();
            services.AddScoped<IDienstRepository, DienstRepository>();
            services.AddScoped<IFeestdagRepository, FeestdagRepository>();
            services.AddScoped<IEffectieveDienstRepository, EffectieveDienstRepository>();
            services.AddScoped<IEffectieveOnderbrekingRepository, EffectieveOnderbrekingRepository>();
            services.AddScoped<IInstellingenRepository, InstellingenRepository>();
            services.AddScoped<IOnderbrekingRepository, OnderbrekingRepository>();
            services.AddScoped<ILoonficheSettingsRepository, LoonficheSettingsRepository>();
            services.AddScoped<ApplicationDataInitialiser>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, ApplicationDbContext dbContext, IHostingEnvironment env, ApplicationDataInitialiser dataInitialiser) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseCors("AllowAll");
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseMvc();
            app.UseSwaggerUi3();
            app.UseSwagger();
            if (env.IsDevelopment()) {
                dataInitialiser.InitializeData();
            } else {
                dbContext.Database.Migrate();
            }
        }
    }
}