﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using GandaCarsAPI.DTO;
using GandaCarsAPI.Models;
using GandaCarsAPI.Models.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GandaCarsAPI.Controllers {
    [EnableCors("AllowAll")]
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [AllowAnonymous]
    public class EffectieveDienstenController : Controller {
        private readonly IDienstRepository _dienstRepository;
        private readonly IEffectieveDienstRepository _effectieveDienstRepository;
        private readonly IBusChauffeurRepository _busChauffeurRepository;
        private readonly IEffectieveOnderbrekingRepository _effectieveOnderbrekingsRepository;

        public EffectieveDienstenController(IEffectieveDienstRepository effectieveDienstRepository, IDienstRepository dienstRepository,
            IBusChauffeurRepository busChauffeurRepository, IEffectieveOnderbrekingRepository effectieveOnderbrekingRepository) {
            _effectieveDienstRepository = effectieveDienstRepository;
            _dienstRepository = dienstRepository;
            _busChauffeurRepository = busChauffeurRepository;
            _effectieveOnderbrekingsRepository = effectieveOnderbrekingRepository;
        }

        [HttpGet("{id}")]
        public ActionResult<EffectieveDienst> GetEffectieveDienstById(string id) {
            EffectieveDienst i = _effectieveDienstRepository.GetBy(id);
            if (i == null) return NotFound("De effectieve dienst met opgegeven id kon niet worden gevonden.");
            return i;
        }

        [HttpPost("{jaar}/{week}/{busChauffeurId}")]
        public ActionResult<IEnumerable<EffectieveDienst>> PostEffectieveDiensten(List<EffectieveDienstDTO> effectieveDienstDTOS, string jaar, string week, string busChauffeurId) {
            string request = null;
            BusChauffeur bc = _busChauffeurRepository.GetBy(busChauffeurId);
            if (bc == null) { return BadRequest("De buschauffeur met opgegeven id kon niet worden gevonden."); }
            CultureInfo myCI = new CultureInfo("nl-BE");
            Calendar myCal = myCI.Calendar;
            CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
            DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;
            foreach (EffectieveDienstDTO d in effectieveDienstDTOS) {
                var weekNummerVanStartDatum = myCal.GetWeekOfYear(d.Start.Value.DateTime, myCWR, myFirstDOW);
                if (!d.BusChauffeurId.Equals(busChauffeurId)) {
                    request = "Id's komen niet overeen!";
                } else if (d.Start.Value.Year != Int32.Parse(jaar)) {
                    request = "Het jaartal van de start van dienst '" + d.Naam + "' komt niet overeen met dat van de week die u aan het bewerken bent!";
                } else if (!(weekNummerVanStartDatum == Int32.Parse(week) || (weekNummerVanStartDatum == 53 && Int32.Parse(week) == 52))) {
                    request = "De start van dienst '" + d.Naam + "' valt niet binnen de week die u aan het bewerken bent!";
                }
                List<EffectieveDienstDTO> teVergelijkenDiensten = effectieveDienstDTOS.Where(x => x != d).ToList();
                foreach (EffectieveDienstDTO vergelijkDienst in teVergelijkenDiensten) {
                    if (d.Naam == vergelijkDienst.Naam && d.Type == DienstType.Dienst) {
                        request = "Er bestaan 2 diensten die '" + d.Naam + "' heten!";
                    }
                    if (d.Type == DienstType.Dienst) { } else {
                        if (d.Type != DienstType.Feestdag && d.Start.Value.Date == vergelijkDienst.Start.Value.Date) {
                            request = "De dienst '" + d.Naam + "' is fout: er kan geen andere dienst plaatsvinden op dezelfde dag als een dienst van type " + d.Type + ". Dienst '" + vergelijkDienst.Naam + "' valt op dezelfde dag.";
                        }
                    }
                    if (d.Start < vergelijkDienst.Start && vergelijkDienst.Start < d.Einde) {
                        request = "Dienst '" + d.Naam + "' en '" + vergelijkDienst.Naam + "' overlappen!";
                    }
                };
            }
            if (request != null) return BadRequest(request);
            List<EffectieveDienst> oudeDataList = _effectieveDienstRepository.DeleteAllVan(bc, jaar, week).ToList();
            var upgeloadeDiensten = new List<EffectieveDienst>();
            effectieveDienstDTOS.ForEach(effectieveDienstDTO => {
                EffectieveDienst hoofdDienst = new EffectieveDienst();
                hoofdDienst.BusChauffeur = bc;
                hoofdDienst.Naam = effectieveDienstDTO.Naam;
                hoofdDienst.AndereMinuten = effectieveDienstDTO.AndereMinuten;
                hoofdDienst.Type = effectieveDienstDTO.Type;
                effectieveDienstDTO.EffectieveOnderbrekingen.ForEach(eoDTO => {
                    EffectieveOnderbreking eo = new EffectieveOnderbreking() {
                    Id = null,
                    Type = eoDTO.Type.Value,
                    EffectieveStart = eoDTO.EffectieveStart.Value,
                    EffectiefEinde = eoDTO.EffectiefEinde.Value
                    };
                    hoofdDienst.EffectieveOnderbrekingen.Add(eo);
                });
                if (effectieveDienstDTO.Start.Value.Date == effectieveDienstDTO.Einde.Value.Date) {
                    hoofdDienst.Start = effectieveDienstDTO.Start.Value.ToLocalTime();
                    hoofdDienst.Einde = effectieveDienstDTO.Einde.Value.ToLocalTime();
                    bc.EffectieveDiensten.Add(hoofdDienst);
                    _effectieveDienstRepository.Add(hoofdDienst);
                } else {
                    hoofdDienst.Start = effectieveDienstDTO.Start.Value.ToLocalTime();
                    hoofdDienst.Einde = effectieveDienstDTO.Einde.Value.Date;
                    EffectieveDienst gerelateerdeDienst = new EffectieveDienst();
                    gerelateerdeDienst.Start = effectieveDienstDTO.Einde.Value.Date;
                    gerelateerdeDienst.Einde = effectieveDienstDTO.Einde.Value.ToLocalTime();
                    gerelateerdeDienst.BusChauffeur = bc;
                    gerelateerdeDienst.Naam = effectieveDienstDTO.Naam;
                    gerelateerdeDienst.Type = effectieveDienstDTO.Type;
                    _effectieveDienstRepository.Add(gerelateerdeDienst);
                    hoofdDienst.GerelateerdeDienst = gerelateerdeDienst;
                    _effectieveDienstRepository.Add(hoofdDienst);
                    _effectieveDienstRepository.SaveChanges();
                    gerelateerdeDienst.GerelateerdeDienst = hoofdDienst;
                    _effectieveDienstRepository.Update(gerelateerdeDienst);
                    bc.EffectieveDiensten.Add(hoofdDienst);
                }
                upgeloadeDiensten.Add(hoofdDienst);
            });
            _effectieveDienstRepository.SaveChanges();
            return upgeloadeDiensten;

        }

        [HttpGet("{jaar}/{week}/{busChauffeurId}")]
        public ActionResult<IEnumerable<EffectieveDienst>> GetEffectievieDienstenVanChauffeur(string jaar, string week, string busChauffeurId) {
            BusChauffeur bc = _busChauffeurRepository.GetBy(busChauffeurId);
            if (bc == null) return BadRequest("De buschauffeur met opgegeven id kon niet worden gevonden.");
            var uitvoer = _effectieveDienstRepository.GetAllVan(bc, jaar, week).ToList();
            return uitvoer;
        }

        [HttpDelete("{id}")]
        public ActionResult<EffectieveDienst> VerwijderDienst(string id) {
            EffectieveDienst ed = _effectieveDienstRepository.GetBy(id);
            if (ed == null) return BadRequest("De effectieve dienst met opgegeven id kon niet worden gevonden.");
            _effectieveDienstRepository.Delete(ed);
            _effectieveDienstRepository.SaveChanges();
            return ed;
        }

        [HttpDelete("verwijderDiensten/{jaar}/{week}/{busChauffeurId}")]
        public ActionResult<List<EffectieveDienst>> VerwijderDienst(string busChauffeurId, string jaar, string week) {
            BusChauffeur bc = _busChauffeurRepository.GetBy(busChauffeurId);
            if (bc == null) return BadRequest("Het buschauffeur met opgegeven id kon niet worden gevonden.");
            var uitvoer = _effectieveDienstRepository.DeleteAllVan(bc, jaar, week).ToList();
            _effectieveDienstRepository.SaveChanges();
            return uitvoer;
        }

        [HttpGet("getAll")]
        public IEnumerable<EffectieveDienst> GetDiensts() {
            return _effectieveDienstRepository.GetAll();
        }

        [HttpGet("getByMonth/{jaar}/{maand}/{busChauffeurId}")]
        public ActionResult<IEnumerable<EffectieveDienst>> GetByMonth(string busChauffeurId, string jaar, int maand) {
            BusChauffeur bc = _busChauffeurRepository.GetBy(busChauffeurId);
            if (bc == null) return BadRequest("Het buschauffeur met opgegeven id kon niet worden gevonden.");
            return _effectieveDienstRepository.GetAllByMonth(bc, jaar, maand).ToList();
        }
    }
}