﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GandaCarsAPI.DTO;
using GandaCarsAPI.Models;
using GandaCarsAPI.Models.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace GandaCarsAPI.Controllers {
    [EnableCors("AllowAll")]
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [AllowAnonymous]
    public class LoonficheSettingController : Controller {
        private readonly ILoonficheSettingsRepository _loonficheSettingsRepository;
        private readonly IBusChauffeurRepository _busChauffeurRepository;
        public LoonficheSettingController(ILoonficheSettingsRepository loonficheSettingsRepository, IBusChauffeurRepository busChauffeurRepository) {
            _loonficheSettingsRepository = loonficheSettingsRepository;
            _busChauffeurRepository = busChauffeurRepository;
        }

        [HttpGet("{id}")]
        public ActionResult<LoonficheSetting> GetById(string id) {
            LoonficheSetting i = _loonficheSettingsRepository.GetById(id);
            if (i == null) return NotFound("De loonficheSetting met opgegeven id kon niet worden gevonden.");
            return i;
        }

        [HttpGet("{bcId}/{jaar}/{maand}")]
        public ActionResult<LoonficheSetting> GetByJaarEnMaand(string bcId, int jaar, int maand) {
            BusChauffeur bc = _busChauffeurRepository.GetBy(bcId);
            if (bc == null) return NotFound("De chauffeur met opgegeven id kon niet worden gevonden.");
            var uitvoer = _loonficheSettingsRepository.GetByYearAndMonth(bc, jaar, maand);
            return uitvoer;
        }

        [HttpDelete("{id}")]
        public ActionResult<LoonficheSetting> DeletetById(string id) {
            LoonficheSetting i = _loonficheSettingsRepository.GetById(id);
            if (i == null) return NotFound("De loonficheSetting met opgegeven id kon niet worden gevonden.");
            BusChauffeur bc = _busChauffeurRepository.GetBy(i.BusChauffeur.Id);
            if (bc == null) return NotFound("De chauffeur met opgegeven id kon niet worden gevonden.");
            bc.loonficheSettings = bc.loonficheSettings.Where(x => x.Id != id).ToList();
            bc.OverUren = bc.OverUren - i.ResterendeUrenDezeMaand + i.OverurenOpnemen;
            _loonficheSettingsRepository.Delete(i);
            _loonficheSettingsRepository.SaveChanges();
            return i;
        }

        [HttpGet("getAll")]
        public IEnumerable<LoonficheSetting> GetAllLoonfiches() {
            return _loonficheSettingsRepository.GetAll();
        }

        [HttpPut("{id}")]
        public ActionResult<LoonficheSetting> PutLoonficheSettings(string id, LoonficheSettingDTO dto) {
            try {
                if (!dto.Id.Equals(id)) {
                    return BadRequest("Id's komen niet overeen!");
                }
                BusChauffeur bc = _busChauffeurRepository.GetBy(dto.BusChauffeurId);
                if (bc == null) return NotFound("De chauffeur met opgegeven id kon niet worden gevonden.");
                LoonficheSetting lfs = _loonficheSettingsRepository.GetById(id);
                bc.OverUren = bc.OverUren - lfs.ResterendeUrenDezeMaand + lfs.OverurenOpnemen + dto.ResterendeUrenDezeMaand.Value - dto.OverurenOpnemen.Value;
                lfs.Jaar = dto.Jaar.Value;
                lfs.Maand = dto.Maand.Value;
                lfs.MilisecondenGewaarborgdLoonEersteWeek = dto.MilisecondenGewaarborgdLoonEersteWeek.Value;
                lfs.MilisecondenGewaarborgdLoonTweedeWeek = dto.MilisecondenGewaarborgdLoonTweedeWeek.Value;
                lfs.MilisecondenGewaarborgdLoonLaatsteDeel = dto.MilisecondenGewaarborgdLoonLaatsteDeel.Value;
                lfs.RSZ = dto.RSZ.Value;
                lfs.Bedrijsvoorheffing = dto.Bedrijsvoorheffing.Value;
                lfs.FietsRitten = dto.FietsRitten.Value;
                lfs.ARABvergoeding = dto.ARABvergoeding.Value;
                lfs.OntvangenVoorschot1 = dto.OntvangenVoorschot1.Value;
                lfs.OntvangenVoorschot2 = dto.OntvangenVoorschot2.Value;
                lfs.VoorschotVolgendeMaand = dto.VoorschotVolgendeMaand.Value;
                lfs.OverurenOpnemen = dto.OverurenOpnemen.Value;
                lfs.ResterendeUrenDezeMaand = dto.ResterendeUrenDezeMaand.Value;
                lfs.Loonbeslag = dto.Loonbeslag.Value;
                lfs.GewerkteUrenDezeMaand = dto.GewerkteUrenDezeMaand.Value;
                _loonficheSettingsRepository.Update(lfs);
                _loonficheSettingsRepository.SaveChanges();
                return lfs;
            } catch (Exception e) {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        public ActionResult<LoonficheSetting> AddLoonficheSettings(LoonficheSettingDTO dto) {
            try {
                BusChauffeur bc = _busChauffeurRepository.GetBy(dto.BusChauffeurId);
                if (bc == null) return NotFound("De chauffeur met opgegeven id kon niet worden gevonden.");
                LoonficheSetting lfs = new LoonficheSetting();
                lfs.BusChauffeur = bc;
                lfs.Jaar = dto.Jaar.Value;
                lfs.Maand = dto.Maand.Value;
                lfs.MilisecondenGewaarborgdLoonEersteWeek = dto.MilisecondenGewaarborgdLoonEersteWeek.Value;
                lfs.MilisecondenGewaarborgdLoonTweedeWeek = dto.MilisecondenGewaarborgdLoonTweedeWeek.Value;
                lfs.MilisecondenGewaarborgdLoonLaatsteDeel = dto.MilisecondenGewaarborgdLoonLaatsteDeel.Value;
                lfs.RSZ = dto.RSZ.Value;
                lfs.Bedrijsvoorheffing = dto.Bedrijsvoorheffing.Value;
                lfs.FietsRitten = dto.FietsRitten.Value;
                lfs.ARABvergoeding = dto.ARABvergoeding.Value;
                lfs.OntvangenVoorschot1 = dto.OntvangenVoorschot1.Value;
                lfs.OntvangenVoorschot2 = dto.OntvangenVoorschot2.Value;
                lfs.VoorschotVolgendeMaand = dto.VoorschotVolgendeMaand.Value;
                lfs.OverurenOpnemen = dto.OverurenOpnemen.Value;
                lfs.ResterendeUrenDezeMaand = dto.ResterendeUrenDezeMaand.Value;
                lfs.Loonbeslag = dto.Loonbeslag.Value;
                lfs.GewerkteUrenDezeMaand = dto.GewerkteUrenDezeMaand.Value;
                bc.OverUren = bc.OverUren + lfs.ResterendeUrenDezeMaand - lfs.OverurenOpnemen;
                bc.loonficheSettings.Add(lfs);
                _loonficheSettingsRepository.Add(lfs);
                _loonficheSettingsRepository.SaveChanges();
                return lfs;
            } catch (Exception e) {
                return BadRequest(e.Message);
            }
        }
    }
}