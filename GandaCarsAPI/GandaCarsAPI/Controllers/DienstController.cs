﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using GandaCarsAPI.DTO;
using GandaCarsAPI.Models;
using GandaCarsAPI.Models.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GandaCarsAPI.Controllers {
    [EnableCors("AllowAll")]
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [AllowAnonymous]
    public class DienstController : Controller {
        private readonly IDienstRepository _dienstRepository;
        private readonly IBusChauffeurRepository _busChauffeurRepository;
        private readonly IOnderbrekingRepository _onderbrekingRepository;

        public DienstController(IDienstRepository dienstRepository,
            IBusChauffeurRepository busChauffeurRepository, IOnderbrekingRepository onderbrekingRepository) {
            _dienstRepository = dienstRepository;
            _busChauffeurRepository = busChauffeurRepository;
            _onderbrekingRepository = onderbrekingRepository;
        }

        [HttpGet("{id}")]
        public ActionResult<Dienst> GetDienstById(string id) {
            Dienst i = _dienstRepository.GetBy(id);
            if (i == null) return NotFound("De dienst met opgegeven id kon niet worden gevonden.");
            return i;
        }

        [HttpGet("tryGetByName/{name}")]
        public ActionResult<List<Dienst>> tryGetByName(string name) {
            return _dienstRepository.TryGetDienstByName(name);
        }

        [HttpDelete("{id}")]
        public ActionResult<Dienst> VerwijderDienst(string id) {
            Dienst g = _dienstRepository.GetBy(id);
            if (g == null) {
                return NotFound("Het item met opgegeven id kon niet worden gevonden.");
            }
            _dienstRepository.Delete(g);
            _dienstRepository.SaveChanges();
            return g;
        }

        [HttpGet("getAll")]
        public IEnumerable<Dienst> GetDiensts() {
            return _dienstRepository.GetAll();
        }

        [HttpPut("{id}")]
        public ActionResult<Dienst> PutDienst(string id, DienstDTO dto) {
            if (!dto.Id.Equals(id)) {
                return BadRequest("id's komen niet overeen!");
            }
            Dienst dienst = _dienstRepository.GetBy(id);
            Dienst checkIfNameExists = _dienstRepository.GetByName(dto.Naam);
            if (checkIfNameExists != null && dto.Naam != checkIfNameExists.Naam) {
                return BadRequest("Er bestaat al een dienst met de naam '" + dto.Naam + "'. Geef een andere naam in!");
            }
            dienst.StartUur = dto.StartUur;
            dienst.EindUur = dto.EindUur;
            dienst.Naam = dto.Naam;
            dienst.StartDag = dto.StartDag;
            dienst.EindDag = dto.EindDag;

            if (dienst.BusChauffeur != null) {
                BusChauffeur OudeBc = _busChauffeurRepository.GetBy(dienst.BusChauffeur.Id);
                OudeBc.Diensten.Remove(OudeBc.Diensten.SingleOrDefault(d => d.Id == dienst.Id));
                _busChauffeurRepository.Update(dienst.BusChauffeur);
                dienst.BusChauffeur = null;
            }
            BusChauffeur NieuweBc = _busChauffeurRepository.GetBy(dto.BusChauffeurId);
            dienst.BusChauffeur = NieuweBc;
            if (NieuweBc != null) {
                NieuweBc.Diensten.Add(dienst);
                _busChauffeurRepository.Update(NieuweBc);
            }

            var nieweOnderbrekingen = dto.Onderbrekingen.Select(x => { return x.asOnderbreking(); }).ToList();
            _onderbrekingRepository.DeleteRange(dienst.Onderbrekingen);
            _onderbrekingRepository.AddRange(nieweOnderbrekingen);
            dienst.Onderbrekingen = nieweOnderbrekingen;
            _dienstRepository.Update(dienst);
            _dienstRepository.SaveChanges();
            return dienst;
        }

        [HttpPost]
        public ActionResult<Dienst> VoegDienstToe(DienstDTO dto) {
            if (dto.BusChauffeurId != null && _busChauffeurRepository.GetBy(dto.BusChauffeurId) == null) {
                return BadRequest("De bus chauffeur met opgegeven id kon niet worden gevonden.");
            }
            if (_dienstRepository.GetByName(dto.Naam) != null) {
                return BadRequest("Er bestaat al een dienst met de naam '" + dto.Naam + "'. Geef een andere naam in!");
            }
            Dienst dienst = new Dienst();
            dienst.StartUur = dto.StartUur;
            dienst.EindUur = dto.EindUur;
            dienst.Naam = dto.Naam;
            dienst.StartDag = dto.StartDag;
            dienst.EindDag = dto.EindDag;
            if (dto.BusChauffeurId != null) {
                dienst.BusChauffeur = _busChauffeurRepository.GetBy(dto.BusChauffeurId);
            }
            dienst.Onderbrekingen.ForEach(t => t.Id = null);
            dienst.Onderbrekingen.AddRange(dto.Onderbrekingen.Select(x => { return x.asOnderbreking(); }).ToList());
            dienst.Onderbrekingen = dto.Onderbrekingen.Select(x => { return x.asOnderbreking(); }).ToList();
            _dienstRepository.Add(dienst);
            if (dienst.BusChauffeur != null) {
                dienst.BusChauffeur.Diensten.Add(dienst);
                _busChauffeurRepository.Update(dienst.BusChauffeur);
            }
            _dienstRepository.SaveChanges();
            return dienst;
        }
    }
}