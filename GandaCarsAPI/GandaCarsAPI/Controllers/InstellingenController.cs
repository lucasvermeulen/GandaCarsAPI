﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using GandaCarsAPI.Data;
using GandaCarsAPI.DTO;
using GandaCarsAPI.Models;
using GandaCarsAPI.Models.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GandaCarsAPI.Controllers {
    [EnableCors("AllowAll")]
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [AllowAnonymous]
    public class InstellingenController : Controller {

        private readonly IInstellingenRepository _instellingenRepository;

        public InstellingenController(IInstellingenRepository instellingenRepository) {
            _instellingenRepository = instellingenRepository;
        }

        [HttpGet]
        public ActionResult<Instellingen> GetInstellingById() {
            return _instellingenRepository.GetInstellingen();
        }

        [HttpPut]
        public ActionResult<Instellingen> PutInstellingen(InstellingenDTO dto) {
            Instellingen i = _instellingenRepository.GetInstellingen();
            i.AantalMinutenAdministratieveTijdVoorDienst = dto.AantalMinutenAdministratieveTijdVoorDienst.Value;
            i.FietsVergoeding = dto.FietsVergoeding.Value;
            i.Nachtwerk = dto.Nachtwerk.Value;
            i.OnderbrekingsVergoeding = dto.OnderbrekingsVergoeding.Value;
            i.Premie = dto.Premie.Value;
            i.Zaterdag = dto.Zaterdag.Value;
            i.AmplitudeMiliseconds = dto.AmplitudeMiliseconds.Value;
            i.NachtwerkStart = dto.NachtwerkStart.Value;
            i.NachtwerkEinde = dto.NachtwerkEinde.Value;
            i.NachtwerkNaStartAdministratieveTijd = dto.NachtwerkNaStartAdministratieveTijd.Value;
            i.NachtwerkVoorEindeAdministratieveTijd = dto.NachtwerkVoorEindeAdministratieveTijd.Value;
            i.WerktijdFeestdag = dto.WerktijdFeestdag.Value;
            i.AantalMinutenAdministratieveTijdVoorDienstMetMeerDan2Onderbrekingen = dto.AantalMinutenAdministratieveTijdVoorDienstMetMeerDan2Onderbrekingen.Value;
            i.DefaultVoorschot1 = dto.DefaultVoorschot1.Value;
            i.DefaultVoorschot2 = dto.DefaultVoorschot2.Value;
            i.DefaultVoorschotVolgendeMaand = dto.DefaultVoorschotVolgendeMaand.Value;
            i.GewaarborgdLoonEersteWeek = dto.GewaarborgdLoonEersteWeek.Value;
            i.GewaarborgdLoonTweedeWeek = dto.GewaarborgdLoonTweedeWeek.Value;
            i.GewaarbordLoonLaatsteDeel = dto.GewaarbordLoonLaatsteDeel.Value;
            i.AantalUrenStandaardWerkdag = dto.AantalUrenStandaardWerkdag.Value;
            i.DefaultGeselecteerdeKolommenBijBuschauffeurTabel = dto.DefaultGeselecteerdeKolommenBijBuschauffeurTabel;
            i.OverurenAanpassen = dto.OverurenAanpassen.Value;
            _instellingenRepository.Update(i);
            _instellingenRepository.SaveChanges();
            return i;
        }
    }
}