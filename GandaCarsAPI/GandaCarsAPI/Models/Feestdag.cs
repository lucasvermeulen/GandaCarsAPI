﻿using System;
namespace GandaCarsAPI.Models
{
    public class Feestdag
    {
        public string Id { get; set; }
        public DateTimeOffset Dag { get; set; }
        public String Naam { get; set; }
        public Feestdag()
        {
        }
    }
}
