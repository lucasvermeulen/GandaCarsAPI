﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace GandaCarsAPI.Models {
    public class LoonficheSetting {
        public string Id { get; set; }
        public BusChauffeur BusChauffeur { get; set; }
        public int Jaar { get; set; }
        public int Maand { get; set; }
        public double MilisecondenGewaarborgdLoonEersteWeek { get; set; }
        public double MilisecondenGewaarborgdLoonTweedeWeek { get; set; }
        public double MilisecondenGewaarborgdLoonLaatsteDeel { get; set; }
        public double RSZ { get; set; }
        public double Bedrijsvoorheffing { get; set; }
        public int FietsRitten { get; set; }
        public double ARABvergoeding { get; set; }
        public double VoorschotVolgendeMaand { get; set; }
        public double OntvangenVoorschot1 { get; set; }
        public double OntvangenVoorschot2 { get; set; }
        public double OverurenOpnemen { get; set; }
        public double ResterendeUrenDezeMaand { get; set; }
        public double Loonbeslag { get; set; }
        public double GewerkteUrenDezeMaand { get; set; }
        public LoonficheSetting() { }
    }
}