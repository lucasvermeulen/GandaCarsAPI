﻿using System;
using System.Collections.Generic;

namespace GandaCarsAPI.Models {
    public class EffectieveDienst {
        public string Id { get; set; }
        public string Naam { get; set; }
        public DateTimeOffset Start { get; set; }
        public DateTimeOffset Einde { get; set; }
        public BusChauffeur BusChauffeur { get; set; }
        public int? AndereMinuten { get; set; }
        public DateTimeOffset DagVanToevoegen { get; set; }
        public EffectieveDienst GerelateerdeDienst { get; set; }
        public List<EffectieveOnderbreking> EffectieveOnderbrekingen { get; set; }
        public DienstType Type { get; set; } = DienstType.Dienst;
        public EffectieveDienst() {
            DagVanToevoegen = DateTime.Now;
            EffectieveOnderbrekingen = new List<EffectieveOnderbreking>();
        }
    }
}