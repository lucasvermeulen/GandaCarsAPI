﻿using System;
using System.Collections.Generic;

namespace GandaCarsAPI.Models {
    public class Dienst {
        public string Id { get; set; }
        public string Naam { get; set; }
        public TimeSpan? StartUur { get; set; }
        public TimeSpan? EindUur { get; set; }
        public int StartDag { get; set; }
        public int EindDag { get; set; }
        public DateTimeOffset DagVanToevoegen { get; set; }
        public BusChauffeur BusChauffeur { get; set; }
        public List<Onderbreking> Onderbrekingen { get; set; }
        public Dienst() {
            DagVanToevoegen = DateTime.Now;
            Onderbrekingen = new List<Onderbreking>();
        }
    }
}