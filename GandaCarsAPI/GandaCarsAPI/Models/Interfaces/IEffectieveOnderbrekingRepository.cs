﻿using System;
using System.Collections.Generic;

namespace GandaCarsAPI.Models.Interfaces
{
    public interface IEffectieveOnderbrekingRepository
    {
        EffectieveOnderbreking GetBy(string id);
        IEnumerable<EffectieveOnderbreking> GetAll();
        void Add(EffectieveOnderbreking ond);
        void Delete(EffectieveOnderbreking ond);
        void AddRange(List<EffectieveOnderbreking> ond);
        void DeleteRange(List<EffectieveOnderbreking> ond);
        void Update(EffectieveOnderbreking ond);
        void SaveChanges();
    }
}
