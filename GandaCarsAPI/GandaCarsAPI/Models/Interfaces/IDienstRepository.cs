﻿using System;
using System.Collections.Generic;

namespace GandaCarsAPI.Models.Interfaces {
    public interface IDienstRepository {
        Dienst GetBy(string id);
        List<Dienst> TryGetDienstByName(string name);
        Dienst GetByName(string naam);
        IEnumerable<Dienst> GetAll();
        List<Dienst> GetAllVan(BusChauffeur bc);
        void Add(Dienst bc);
        void Delete(Dienst dienst);
        void Update(Dienst dienst);
        void SaveChanges();
    }
}