﻿using System;
using System.Collections.Generic;

namespace GandaCarsAPI.Models.Interfaces {
    public interface IFeestdagRepository {
        List<Feestdag> GetAll();
        void AddRange(IEnumerable<Feestdag> fden);
        void DeleteRange(IEnumerable<Feestdag> fden);
        void SaveChanges();
    }
}