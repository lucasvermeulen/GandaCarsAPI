﻿using System;
using System.Collections.Generic;
using GandaCarsAPI.DTO;

namespace GandaCarsAPI.Models.Interfaces {
    public interface ILoonficheSettingsRepository {
        IEnumerable<LoonficheSetting> GetAll();
        LoonficheSetting GetById(string id);
        LoonficheSetting GetByYearAndMonth(BusChauffeur bc, int jaar, int maand);
        LoonficheSetting Add(LoonficheSetting lfs);
        LoonficheSetting Update(LoonficheSetting lfs);
        LoonficheSetting Delete(LoonficheSetting lfs);
        void SaveChanges();
    }
}