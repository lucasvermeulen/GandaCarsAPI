﻿using System;
namespace GandaCarsAPI.Models {
    public class Onderbreking {
        public String Id { get; set; }
        public TimeSpan StartUur { get; set; }
        public TimeSpan EindUur { get; set; }
        public int StartDag { get; set; }
        public int EindDag { get; set; }
        public OnderbrekingsType Type { get; set; }
        public Onderbreking() { }
    }
}