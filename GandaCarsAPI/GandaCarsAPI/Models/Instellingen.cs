﻿using System;
namespace GandaCarsAPI.Models {
    public class Instellingen {
        public string Id { get; set; }
        public int AantalMinutenAdministratieveTijdVoorDienst { get; set; } = 900000;
        public Double FietsVergoeding { get; set; } = 0.21;
        public Double Nachtwerk { get; set; } = 1.56;
        public Double NachtwerkStart { get; set; } = 72000000;
        public Double NachtwerkEinde { get; set; } = 21600000;
        public Double NachtwerkNaStartAdministratieveTijd { get; set; } = 600000;
        public Double NachtwerkVoorEindeAdministratieveTijd { get; set; } = 300000;
        public Double OnderbrekingsVergoeding { get; set; } = 3.07;
        public Double Premie { get; set; } = 150.48;
        public Double Zaterdag { get; set; } = 25;
        public Double AmplitudeMiliseconds { get; set; } = 43200000;
        public Double WerktijdFeestdag { get; set; } = 22200000;
        public Double AantalMinutenAdministratieveTijdVoorDienstMetMeerDan2Onderbrekingen { get; set; } = 5400000;
        public Double DefaultVoorschot1 { get; set; } = 550;
        public Double DefaultVoorschot2 { get; set; } = 550;
        public Double DefaultVoorschotVolgendeMaand { get; set; } = 550;
        public Double GewaarborgdLoonEersteWeek { get; set; } = 100;
        public Double GewaarborgdLoonTweedeWeek { get; set; } = 85.88;
        public Double GewaarbordLoonLaatsteDeel { get; set; } = 25.88;
        public Double AantalUrenStandaardWerkdag { get; set; } = 22200000;
        public String DefaultGeselecteerdeKolommenBijBuschauffeurTabel { get; set; } = "[{\"field\":\"voornaam\",\"header\":\"Voornaam\"},{\"field\":\"achternaam\",\"header\":\"Achternaam\"},{\"field\":\"uurloon\",\"header\":\"Uurloon\"},{\"field\":\"geboorteDatum\",\"header\":\"Geboortedatum\"},{\"field\":\"afstandKM\",\"header\":\"Afstand(KM)\"},{\"field\":\"stelsel.id\",\"header\":\"Stelsel\"},{\"field\":\"overUren\",\"header\":\"Overuren\"},{\"field\":\"aantalDagenJaarlijksVerlof\",\"header\":\"AantaldagenJV\"}]";
        public Boolean OverurenAanpassen { get; set; } = false;
        public Instellingen() {
            AantalMinutenAdministratieveTijdVoorDienst = 900000;
            AantalMinutenAdministratieveTijdVoorDienstMetMeerDan2Onderbrekingen = 5400000;
            DefaultVoorschot1 = 550;
            DefaultVoorschot2 = 550;
            DefaultVoorschotVolgendeMaand = 550;
            GewaarborgdLoonEersteWeek = 100;
            GewaarborgdLoonTweedeWeek = 85.88;
            AantalUrenStandaardWerkdag = 22200000;
            GewaarbordLoonLaatsteDeel = 25.88;
            FietsVergoeding = 0.21;
            Nachtwerk = 1.56;
            OnderbrekingsVergoeding = 3.07;
            Premie = 150.48;
            Zaterdag = 25;
            AmplitudeMiliseconds = 43200000;
            NachtwerkStart = 72000000;
            NachtwerkEinde = 21600000;
            NachtwerkNaStartAdministratieveTijd = 600000;
            NachtwerkVoorEindeAdministratieveTijd = 300000;
            WerktijdFeestdag = 22200000;
            DefaultGeselecteerdeKolommenBijBuschauffeurTabel = "[{\"field\":\"voornaam\",\"header\":\"Voornaam\"},{\"field\":\"achternaam\",\"header\":\"Achternaam\"},{\"field\":\"uurloon\",\"header\":\"Uurloon\"},{\"field\":\"geboorteDatum\",\"header\":\"Geboortedatum\"},{\"field\":\"afstandKM\",\"header\":\"Afstand(KM)\"},{\"field\":\"stelsel.id\",\"header\":\"Stelsel\"},{\"field\":\"overUren\",\"header\":\"Overuren\"},{\"field\":\"aantalDagenJaarlijksVerlof\",\"header\":\"AantaldagenJV\"}]";
            OverurenAanpassen = false;
        }
    }
}