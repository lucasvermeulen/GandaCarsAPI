﻿using System;
namespace GandaCarsAPI.Models
{
    public class EffectieveOnderbreking
    {
        public String Id { get; set; }
        public DateTimeOffset EffectieveStart { get; set; }
        public DateTimeOffset EffectiefEinde { get; set; }
        public OnderbrekingsType Type { get; set; }
        public EffectieveOnderbreking()
        {
        }
    }
}
