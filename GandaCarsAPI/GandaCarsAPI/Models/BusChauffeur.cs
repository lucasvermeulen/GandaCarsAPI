﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GandaCarsAPI.Models {
    public class BusChauffeur {
        public string Id { get; set; }
        public string Voornaam { get; set; }
        public string Achternaam { get; set; }
        public Double Uurloon { get; set; }
        public string Straatnaam { get; set; }
        public string Huisnummer { get; set; }
        public string Postcode { get; set; }
        public string Stad { get; set; }
        public string Land { get; set; }
        public DateTimeOffset GeboorteDatum { get; set; }
        public int Stelsel { get; set; }
        public List<Dienst> Diensten { get; set; }
        public List<EffectieveDienst> EffectieveDiensten { get; set; }
        public Double AfstandKM { get; set; }
        public Double OverUren { get; set; }
        public int AantalDagenJaarlijksVerlof { get; set; } = 24;
        public List<LoonficheSetting> loonficheSettings { get; set; }
        public BusChauffeur() {
            Diensten = new List<Dienst>();
            EffectieveDiensten = new List<EffectieveDienst>();
            loonficheSettings = new List<LoonficheSetting>();
        }
    }
}