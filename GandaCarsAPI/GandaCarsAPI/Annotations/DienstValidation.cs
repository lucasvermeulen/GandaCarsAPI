using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using GandaCarsAPI.DTO;
using GandaCarsAPI.Models;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
public class DienstValidation : ValidationAttribute {

    protected override ValidationResult IsValid(object value, ValidationContext validationContext) {
        try {
            //check property's before casting

            if (value.GetType().GetProperty("StartUur").GetValue(value) == null) {
                return new ValidationResult("Het startuur van een dienst, van het type 'Dienst', is verplicht!");
            }
            if (value.GetType().GetProperty("EindUur").GetValue(value) == null) {
                return new ValidationResult("Het einduur van een dienst, van het type 'Dienst', is verplicht!");
            }
            DienstDTO dienst = (DienstDTO) value;

            //validatie dienst --> is de dienst zelf geldig?
            if ((dienst.StartDag > dienst.EindDag) && !(dienst.StartDag == 7 && dienst.EindDag == 1) || (dienst.EindDag - dienst.StartDag > 1) && !(dienst.StartDag == 7 && dienst.EindDag == 1)) {
                return new ValidationResult("De data van de dienst '" + dienst.Naam + "' zijn fout!");
            } else {
                if (dienst.StartDag == dienst.EindDag) {
                    if (dienst.StartUur.Value > dienst.EindUur.Value) {
                        return new ValidationResult("De start tijd van de dienst '" + dienst.Naam + "' valt voor de eind tijd!");
                    }
                } else {
                    if (dienst.StartUur.Value < dienst.EindUur.Value) {
                        return new ValidationResult("Dienst '" + dienst.Naam + "' duurt te lang!)");
                    }
                }
                //startUur en eindUur verplicht bij dienst van type Dienst
                if (dienst.StartUur == null) {
                    return new ValidationResult("Het startuur van een dienst, van het type 'Dienst', is verplicht!");
                } else if (dienst.EindUur == null) {
                    return new ValidationResult("Het einduur van een dienst, van het type 'Dienst', is verplicht!");
                }
            }
            ValidationResult fout = null;
            //onderbrekingscontrole
            foreach (OnderbrekingDTO onderbreking in dienst.Onderbrekingen) {
                // verschil in dagen mag niet groter zijn dan 1 (behalve bij 1 & 7!)
                //startdag 3 einddag 4 -> juist / startdag 3 einddag 5 -> fout / startdag 4 einddag 3 -> fout /startdag 5 einddag 3 -> fout
                if ((onderbreking.StartDag > onderbreking.EindDag) && !(onderbreking.StartDag == 7 && onderbreking.EindDag == 1) || (onderbreking.EindDag - onderbreking.StartDag > 1) && !(onderbreking.StartDag == 7 && onderbreking.EindDag == 1)) {
                    fout = new ValidationResult("De onderbreking" + this.getOnderbringsReferentie(onderbreking) +
                        "van dienst '" + dienst.Naam + "' duurt te lang!");
                    break;
                }
                //startdag onderbreking op zelfde dag(en) van dienst
                if (onderbreking.StartDag != dienst.StartDag && onderbreking.StartDag != dienst.EindDag) {
                    fout = new ValidationResult("De startdag van de onderbreking" + this.getOnderbringsReferentie(onderbreking) +
                        "komt niet overeen met die van dienst(" + dienst.Naam + ").");
                    break;
                }
                //eindag onderbreking op zelfde dag(en) van dienst
                if (onderbreking.EindDag != dienst.StartDag && onderbreking.EindDag != dienst.EindDag) {
                    fout = new ValidationResult("De einddag van de onderbreking" + this.getOnderbringsReferentie(onderbreking) +
                        "komt niet overeen met die van dienst(" + dienst.Naam + ").");
                    break;
                }
                //starttijd voor eindtijd van onderbreking op zelfde dag
                if ((onderbreking.EindDag == onderbreking.StartDag) && (onderbreking.StartUur.Value > onderbreking.EindUur.Value)) {
                    fout = new ValidationResult("De start tijd van de onderbreking" + this.getOnderbringsReferentie(onderbreking) + "van dienst '" + dienst.Naam + "' valt voor de eind tijd!");
                    break;
                }
                //startuur onderbreking voor begin dienst
                if (onderbreking.StartDag == dienst.StartDag && onderbreking.StartUur.Value < dienst.StartUur.Value) {
                    fout = new ValidationResult("Het startuur van de onderbreking" + this.getOnderbringsReferentie(onderbreking) +
                        "ligt voor het begin van zijn dienst (" + dienst.Naam + ").");
                    break;
                }
                //einduur onderbreking na einde dienst
                if (onderbreking.EindDag == dienst.EindDag && onderbreking.EindUur.Value > dienst.EindUur.Value) {
                    fout = new ValidationResult("Het einduur van de onderbreking " + this.getOnderbringsReferentie(onderbreking) +
                        " ligt na het eind van zijn dienst (" + dienst.Naam + ").");
                    break;
                }
                //dienst op dezelfde dagen
                if (dienst.StartDag == dienst.EindDag) { }
                //dienst op verschillende dagen
                else if (dienst.StartDag != dienst.EindDag) {
                    //starttijd voor eindtijd van onderbreking op verschillend dag
                    if (onderbreking.StartDag == onderbreking.EindDag && onderbreking.StartUur.Value > onderbreking.EindUur.Value) {
                        fout = new ValidationResult("De tijdstippen van de onderbreking" + this.getOnderbringsReferentie(onderbreking) + "van dienst '" + dienst.Naam + "' zijn fout!");
                        break;
                    }
                    //
                    if (dienst.StartDag == onderbreking.StartDag && onderbreking.StartUur.Value < dienst.StartUur.Value) {
                        fout = new ValidationResult("Het start uur van de onderbreking" + this.getOnderbringsReferentie(onderbreking) + "van dienst '" + dienst.Naam + "' valt voor het start uur van de dienst.");
                    } else if (dienst.EindDag == onderbreking.EindDag && onderbreking.EindUur.Value > dienst.EindUur.Value) {
                        fout = new ValidationResult("Het eind uur van de onderbreking" + this.getOnderbringsReferentie(onderbreking) + "van dienst '" + dienst.Naam + "' valt na het eind uur van de dienst.");
                    }
                }
                List<OnderbrekingDTO> teVergelijkenOnderbrekingen = dienst.Onderbrekingen.Where(x => x != onderbreking).ToList();
                //onderbreking vergelijken met andere onderbrekinge
                foreach (OnderbrekingDTO vergelijkOnderbreking in teVergelijkenOnderbrekingen) {
                    //als startuur onderbreking voor startuur andere onderbreking is moet einduur er ook voor zijn
                    if (onderbreking.StartUur.Value < vergelijkOnderbreking.StartUur.Value && onderbreking.EindUur.Value > vergelijkOnderbreking.StartUur.Value) {
                        fout = new ValidationResult("De onderbreking" + this.getOnderbringsReferentie(onderbreking) + "(van dienst \'" + dienst.Naam + "\') valt tijdens een andere onderbreking!");
                        break;
                    }

                    //onderbreking valt op 2 dagen
                    if (onderbreking.StartDag != onderbreking.EindDag) {
                        //2 onderbrekingen mogen nooit allebei op dag 1 starten en op dag 2 eindigen
                        if (onderbreking.StartDag == vergelijkOnderbreking.StartDag && onderbreking.EindDag == vergelijkOnderbreking.EindDag) {
                            fout = new ValidationResult("De onderbreking" + this.getOnderbringsReferentie(onderbreking) + "(van dienst \'" + dienst.Naam + "\') valt tijdens een andere onderbreking!");
                            break;
                        }
                        // startuur dienst op 2 dagen valt voor startuur dienst op eerste dag
                        if (onderbreking.StartDag == vergelijkOnderbreking.StartDag && onderbreking.StartUur.Value < vergelijkOnderbreking.StartUur.Value) {
                            fout = new ValidationResult("De onderbreking" + this.getOnderbringsReferentie(onderbreking) + "(van dienst \'" + dienst.Naam + "\') valt tijdens een andere onderbreking!");
                            break;
                        }

                        if (onderbreking.EindDag == vergelijkOnderbreking.EindDag && vergelijkOnderbreking.StartUur.Value < onderbreking.EindUur.Value) {
                            fout = new ValidationResult("De onderbreking" + this.getOnderbringsReferentie(onderbreking) + "(van dienst \'" + dienst.Naam + "\') valt tijdens een andere onderbreking!");
                            break;
                        }
                    }
                }
            }

            if (fout != null) {
                return fout;
            }
            return ValidationResult.Success;

        } catch (Exception e) {
            //return ValidationResult.Success;
            return new ValidationResult("Er liep iets fout bij de omzetting van de dienst!");
        }
    }
    private string getOnderbringsReferentie(OnderbrekingDTO o) {
        String dag = "";
        if (o.StartDag == 1) {
            dag = "maandag";
        } else if (o.StartDag == 2) {
            dag = "dinsdag";
        } else if (o.StartDag == 3) {
            dag = "woensdag";
        } else if (o.StartDag == 4) {
            dag = "donderdag";
        } else if (o.StartDag == 5) {
            dag = "vrijdag";
        } else if (o.StartDag == 6) {
            dag = "zaterdag";
        } else if (o.StartDag == 7) {
            dag = "zondag";
        }
        TimeSpan uur = o.StartUur.Value;
        string Hours = "";
        string Minutes = "";
        if (uur.Hours.ToString().Length == 1) {
            Hours = "0";
        }
        if (uur.Minutes.ToString().Length == 1) {
            Minutes = "0";
        }
        Hours += uur.Hours;
        Minutes += uur.Minutes;
        return " die start op " + dag + " om " + uur.Hours + ":" + uur.Minutes + " ";

    }

}