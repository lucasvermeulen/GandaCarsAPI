using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using GandaCarsAPI.DTO;
using GandaCarsAPI.Models;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
public class EffectieveDienstValidation : ValidationAttribute {

    public string FileNotFoundMessage { get; set; }

    private string getOnderbringsReferentie(EffectieveOnderbrekingDTO o) {
        DateTimeOffset start = o.EffectieveStart.Value.DateTime.ToLocalTime();
        return " die start op " + start.ToString("dd/MM/yyyy") + " om " + start.ToString("HH:mm") + " ";
    }
    private string getDienstReferentie(EffectieveDienstDTO d) {
        DateTimeOffset start = d.Start.Value.DateTime.ToLocalTime();
        return " die start op " + start.ToString("dd/MM/yyyy") + " om " + start.ToString("HH:mm") + " ";
    }
    protected override ValidationResult IsValid(object value, ValidationContext validationContext) {
        try {
            EffectieveDienstDTO dienst = (EffectieveDienstDTO) value;
            if (dienst.Start > dienst.Einde) {
                return new ValidationResult("De start van de dienst '" + dienst.Naam + "' valt voor het einde!");
            } else if ((dienst.Einde.Value - dienst.Start.Value) > new TimeSpan(1, 0, 0, 0)) {
                return new ValidationResult("De dienst '" + dienst.Naam + "' duurt te lang!");
            }
            if (dienst.Type == DienstType.Dienst) {
                if (dienst.Start == null) {
                    return new ValidationResult("Geef een geldige start in voor dienst '" + dienst.Naam + "'!");
                } else if (dienst.Einde == null) {
                    return new ValidationResult("Geef een gelid einde in voor dienst '" + dienst.Naam + "'!");
                } else if (dienst.AndereMinuten == null || dienst.AndereMinuten < 0) {
                    return new ValidationResult("Geef een geldige waarde in voor de andere minuten van dienst '" + dienst.Naam + "'!");
                } else if (dienst.Einde.Value.DateTime == dienst.Start.Value.DateTime) {
                    return new ValidationResult("De dienst '" + dienst.Naam + "' start en eindigd op hetzelfde moment!");
                }
                ValidationResult fout = null;
                foreach (EffectieveOnderbrekingDTO onderbreking in dienst.EffectieveOnderbrekingen) {
                    // valideert onderbreking
                    if (onderbreking.EffectieveStart.Value.DateTime > onderbreking.EffectiefEinde.Value.DateTime) {
                        fout = new ValidationResult("Het einde van de onderbreking" + this.getOnderbringsReferentie(onderbreking) + "van dienst'" + dienst.Naam + "' ligt voor de start van de onderbreking.");
                        break;
                    }
                    //onderbreking start en eindigd op hetzelfde moment
                    else if (dienst.Einde.Value.DateTime == dienst.Start.Value.DateTime) {
                        fout = new ValidationResult("De start en het einde van de onderbreking" + this.getOnderbringsReferentie(onderbreking) + "van dienst'" + dienst.Naam + "' liggen op hetzelfde moment!");
                        break;
                    }
                    //valideert dienst met onderbreking
                    else if (onderbreking.EffectieveStart.Value.DateTime < dienst.Start.Value.DateTime) {
                        fout = new ValidationResult("De start van de onderbreking" + this.getOnderbringsReferentie(onderbreking) + "van dienst'" + dienst.Naam + "' ligt voor de start van de dienst!");
                        break;
                    } else if (dienst.Einde.Value.DateTime < onderbreking.EffectiefEinde.Value.DateTime) {
                        fout = new ValidationResult("Het einde van de onderbreking" + this.getOnderbringsReferentie(onderbreking) + "van dienst'" + dienst.Naam + "' ligt na het einde van de dienst!");
                        break;
                    }
                    List<EffectieveOnderbrekingDTO> teVergelijkenOnderbrekingen = dienst.EffectieveOnderbrekingen.Where(x => x != onderbreking).ToList();
                    foreach (EffectieveOnderbrekingDTO vergelijkOnderbreking in teVergelijkenOnderbrekingen) {
                        if (onderbreking.EffectieveStart.Value.DateTime < vergelijkOnderbreking.EffectieveStart.Value.DateTime && vergelijkOnderbreking.EffectieveStart.Value.DateTime < onderbreking.EffectiefEinde.Value.DateTime) {
                            fout = new ValidationResult("2 onderbrekingen van dienst'" + dienst.Naam + "' overlappen!");
                            break;
                        }
                    }
                }
                if (fout != null) {
                    return fout;
                }
            } else {
                if (dienst.EffectieveOnderbrekingen.Count > 0) {
                    return new ValidationResult("Onderbrekingen op een dienst van het type " + dienst.Type + " zijn niet toegelaten!");
                } else if (dienst.Start.Value.Date != dienst.Einde.Value.Date) {
                    return new ValidationResult("De start- en einddatum van een dienst van het type " + dienst.Type + " moeten gelijk zijn!");
                }
            }
            //dienstne controleren
            return ValidationResult.Success;
        } catch {
            //return ValidationResult.Success;
            return new ValidationResult("Er liep iets fout bij de omzetting van de dienst!");
        }
    }
}