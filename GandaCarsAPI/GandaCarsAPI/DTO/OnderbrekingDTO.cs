﻿using System;
using System.ComponentModel.DataAnnotations;
using GandaCarsAPI.Models;

namespace GandaCarsAPI.DTO {
    public class OnderbrekingDTO {
        public String Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Geef een geldig startuur voor de onderbreking in!")]
        public TimeSpan? StartUur { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Geef een geldig einduur voor de onderbreking in!")]
        public TimeSpan? EindUur { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Range(1, 7, ErrorMessage = "Geef een geldige startdag voor de onderbreking in!")]
        public int StartDag { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Range(1, 7, ErrorMessage = "Geef een geldige einddag voor de onderbreking in!")]
        public int EindDag { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Range(1, 2, ErrorMessage = "Geef een geldig type voor de onderbreking in!")]
        public OnderbrekingsType Type { get; set; }
        public OnderbrekingDTO() { }

        public Onderbreking asOnderbreking() {
            Onderbreking o = new Onderbreking();
            if (this.Id != null) {
                o.Id = Id;
            }
            o.StartUur = this.StartUur.Value;
            o.EindUur = this.EindUur.Value;
            o.StartDag = this.StartDag;
            o.EindDag = this.EindDag;
            o.Type = this.Type;
            return o;
        }
    }
}