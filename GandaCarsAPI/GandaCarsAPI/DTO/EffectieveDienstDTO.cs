﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GandaCarsAPI.Models;

namespace GandaCarsAPI.DTO {
    [EffectieveDienstValidation]
    public class EffectieveDienstDTO {
        public string Id { get; set; }

        [MinLength(1, ErrorMessage = "naam")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Geef een geldige naam in!")]
        public string Naam { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Geef een geldige start in!")]
        public DateTimeOffset? Start { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Geef een geldig einde in!")]
        public DateTimeOffset? Einde { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Er is geen buschauffeur id aanwezig!")]
        public String BusChauffeurId { get; set; }

        public int? AndereMinuten { get; set; }
        public List<EffectieveOnderbrekingDTO> EffectieveOnderbrekingen { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Range(1, 5, ErrorMessage = "type")]
        public DienstType Type { get; set; }

        public EffectieveDienstDTO() {
            EffectieveOnderbrekingen = new List<EffectieveOnderbrekingDTO>();
        }
    }
}