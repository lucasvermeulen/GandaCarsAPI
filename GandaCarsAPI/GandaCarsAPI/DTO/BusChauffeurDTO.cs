﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GandaCarsAPI.DTO {
    public class BusChauffeurDTO {
        public string Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Geef een geldige voornaam in!")]
        public string Voornaam { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Geef een geldige achternaam in!")]
        public string Achternaam { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Geef een geldige straatnaam in!")]
        public string Straatnaam { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Geef een geldig huisnummer in!")]
        public string Huisnummer { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Geef een geldige postcode in!")]
        public string Postcode { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Geef een geldige stad in!")]
        public string Stad { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Geef een geldig land in!")]
        public string Land { get; set; }

        [Required(ErrorMessage = "Geef een geldige geboortedatum in!")]
        public DateTimeOffset? GeboorteDatum { get; set; }

        [Required(ErrorMessage = "Geef een geldig uurloon in!")]
        [Range(0, int.MaxValue, ErrorMessage = "Geef een geldig uurloon in!")]
        public double? Uurloon { get; set; }

        [Required(ErrorMessage = "Geef een geldig stelsel in!")]
        [Range(1, 5, ErrorMessage = "Geef een geldig stelsel in!")]
        public int? Stelsel { get; set; }

        [Required(ErrorMessage = "Geef een geldige afstand in!")]
        [Range(0, int.MaxValue, ErrorMessage = "Geef een geldige afstand in!")]
        public double? AfstandKM { get; set; }

        [Required(ErrorMessage = "Geef een geldig aantal overuren in!")]
        public double? OverUren { get; set; }

        [Required(ErrorMessage = "Geef een geldig aantal dagen jaarlijksverlof in!")]
        [Range(0, int.MaxValue, ErrorMessage = "aantaldagenjaarlijksverlof")]
        public int? AantalDagenJaarlijksVerlof { get; set; }

    }
}