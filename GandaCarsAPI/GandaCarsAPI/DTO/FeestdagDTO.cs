﻿using System;
using System.ComponentModel.DataAnnotations;
using NJsonSchema.Annotations;

namespace GandaCarsAPI.DTO {
    public class FeestdagDTO {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Geef een geldige dag in!")]
        public DateTimeOffset? Dag { get; set; }

        [MinLength(1, ErrorMessage = "Geen een geldige feestdag naam in!")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Geen een geldige feestdag naam in!")]
        public String Naam { get; set; }
    }
}