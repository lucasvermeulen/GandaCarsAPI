﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GandaCarsAPI.DTO {
    public class LoonficheSettingDTO {
        public string Id { get; set; }

        [Required(ErrorMessage = "Dit request bevat geen buschauffeur id!")]
        public string BusChauffeurId { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor jaar op!")]
        [Range(0, int.MaxValue, ErrorMessage = "Geef een geldige waarde voor jaar op!")]

        public int? Jaar { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor maand op!")]
        [Range(0, int.MaxValue, ErrorMessage = "Geef een geldige waarde voor maand op!")]

        public int? Maand { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor uren ziekte 1ste week op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor uren ziekte 1ste week op!")]

        public Double? MilisecondenGewaarborgdLoonEersteWeek { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor uren ziekte 2de week op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor uren ziekte 2de week op!")]

        public Double? MilisecondenGewaarborgdLoonTweedeWeek { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor uren ziekte laatste deel op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor uren ziekte laatste deel op!")]

        public Double? MilisecondenGewaarborgdLoonLaatsteDeel { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor RSZ op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor RSZ op!")]

        public Double? RSZ { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor bedrijfsvoorheffing op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor bedrijfsvoorheffing op!")]

        public Double? Bedrijsvoorheffing { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor fietsritten op!")]
        [Range(0, int.MaxValue, ErrorMessage = "Geef een geldige waarde voor fietsritten op!")]

        public int? FietsRitten { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor ARABvergoeding op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor ARABvergoeding op!")]

        public Double? ARABvergoeding { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor voorschot volgende maand op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor voorschot volgende maand op!")]

        public Double? VoorschotVolgendeMaand { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor onvangen voorschot 1 op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor onvangen voorschot 1 op!")]

        public Double? OntvangenVoorschot1 { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor ontvangen voorschot 2 op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor ontvangen voorschot 2 op!")]

        public Double? OntvangenVoorschot2 { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor overuren opnemen op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor overuren opnemen op!")]

        public Double? OverurenOpnemen { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor resterende uren deze maand op!")]
        public Double? ResterendeUrenDezeMaand { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor loonbeslag op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor loonbeslag op!")]

        public Double? Loonbeslag { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor gewerkte uren deze maand op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor gewerkte uren deze maand op!")]

        public Double? GewerkteUrenDezeMaand { get; set; }

    }
}