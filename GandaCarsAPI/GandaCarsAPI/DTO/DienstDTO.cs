﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GandaCarsAPI.Models;
using NJsonSchema.Annotations;

namespace GandaCarsAPI.DTO {
    [DienstValidation]
    public class DienstDTO {
        public string Id { get; set; }

        [MinLength(1, ErrorMessage = "Geef een geldige naam voor de dienst op!")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Geef een geldige naam voor de dienst op!")]
        public string Naam { get; set; }
        public TimeSpan? StartUur { get; set; }
        public TimeSpan? EindUur { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Range(1, 7, ErrorMessage = "Geef een geldige stardag op voor de dienst!")]
        public int StartDag { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Range(1, 7, ErrorMessage = "Geef een geldige einddag op voor de dienst!")]
        public int EindDag { get; set; }

        public string BusChauffeurId { get; set; }

        [Required(AllowEmptyStrings = false)]
        public List<OnderbrekingDTO> Onderbrekingen { get; set; }

        public DienstDTO() {
            Onderbrekingen = new List<OnderbrekingDTO>();
        }
    }
}