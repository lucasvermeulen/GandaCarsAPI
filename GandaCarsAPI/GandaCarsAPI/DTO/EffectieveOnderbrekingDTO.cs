﻿using System;
using System.ComponentModel.DataAnnotations;
using GandaCarsAPI.Models;
using NJsonSchema.Annotations;

namespace GandaCarsAPI.DTO {
    public class EffectieveOnderbrekingDTO {
        public String Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "De start van een onderbreking is verplicht in te vullen!")]
        public DateTimeOffset? EffectieveStart { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Het einde van een onderbreking is verplicht in te vullen!")]
        public DateTimeOffset? EffectiefEinde { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Geef een geldige type op bij elke onderbreking!")]
        [Range(1, 2, ErrorMessage = "Geef een geldige type op bij elke onderbreking!")]
        public OnderbrekingsType? Type { get; set; }
        public EffectieveOnderbrekingDTO() { }

        public EffectieveOnderbreking asEffectieveOnderbreking() {
            EffectieveOnderbreking o = new EffectieveOnderbreking();
            if (this.Id != null) {
                o.Id = Id;
            }
            o.EffectieveStart = this.EffectieveStart.Value;
            o.EffectiefEinde = this.EffectiefEinde.Value;
            o.Type = this.Type.Value;
            return o;
        }
    }
}