﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GandaCarsAPI.DTO {
    public class InstellingenDTO {
        public string Id { get; set; }

        [Required(ErrorMessage = "Geef een geldig aantal minuten administratieve tijd voor dienst op!")]
        [Range(0, int.MaxValue, ErrorMessage = "Geef een geldig aantal minuten administratieve tijd voor dienst op!")]
        public int? AantalMinutenAdministratieveTijdVoorDienst { get; set; }

        [Required(ErrorMessage = "Geef een geldige fietsvergoeding op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige fietsvergoeding op!")]
        public Double? FietsVergoeding { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor nachtwerk op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor nachtwerk op!")]

        public Double? Nachtwerk { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor start nachtwerk op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor start nachtwerk op!")]

        public Double? NachtwerkStart { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor einde nachtwerk op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor einde nachtwerk op!")]

        public Double? NachtwerkEinde { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor administratieve tijd nachtwerk `s ochtends op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor administratieve tijd nachtwerk `s ochtends op!")]

        public Double? NachtwerkNaStartAdministratieveTijd { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde op voor administratieve tijd nachtwerk `s avonds!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde op voor administratieve tijd nachtwerk `s avonds!")]

        public Double? NachtwerkVoorEindeAdministratieveTijd { get; set; }

        [Required(ErrorMessage = "Geef een geldige onderbrekingsvergeoding op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige onderbrekingsvergeoding op!")]

        public Double? OnderbrekingsVergoeding { get; set; }

        [Required(ErrorMessage = "Geef een geldige premie op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige premie op!")]

        public Double? Premie { get; set; }

        [Required(ErrorMessage = "Geef een geldige zaterdagsvergoeding op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige zaterdagsvergoeding op!")]

        public Double? Zaterdag { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde op voor amplitude!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde op voor amplitude!")]

        public Double? AmplitudeMiliseconds { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor werktijd feestdag op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor werktijd feestdag op!")]

        public Double? WerktijdFeestdag { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde op voor aantal administratieve minuten voor dienst met 2 onderbrekingen!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde op voor aantal administratieve minuten voor dienst met 2 onderbrekingen!")]

        public Double? AantalMinutenAdministratieveTijdVoorDienstMetMeerDan2Onderbrekingen { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde op voor default voorschot 1!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde op voor default voorschot 1!")]

        public Double? DefaultVoorschot1 { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde op voor default voorschot 2!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde op voor default voorschot 2!")]

        public Double? DefaultVoorschot2 { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor default voorschot volgende maand op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor default voorschot volgende maand op!")]

        public Double? DefaultVoorschotVolgendeMaand { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor gewaarborgd loon eerste week op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor gewaarborgd loon eerste week op!")]

        public Double? GewaarborgdLoonEersteWeek { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor gewaarborgd loon tweede week op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor gewaarborgd loon tweede week op!")]

        public Double? GewaarborgdLoonTweedeWeek { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor gewaarborgd loon laatste deel op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor gewaarborgd loon laatste deel op!")]

        public Double? GewaarbordLoonLaatsteDeel { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde voor aantal uren voor werkdag op!")]
        [Range(0, double.MaxValue, ErrorMessage = "Geef een geldige waarde voor aantal uren voor werkdag op!")]

        public Double? AantalUrenStandaardWerkdag { get; set; }

        [Required(ErrorMessage = "Geef een geldig waarde voor default geselecteerde kolommen bij buschauffeur tabel op!")]

        public String DefaultGeselecteerdeKolommenBijBuschauffeurTabel { get; set; }

        [Required(ErrorMessage = "Geef een geldige waarde op voor overuren bus chauffeur bewerken!")]
        public Boolean? OverurenAanpassen { get; set; }
    }
}