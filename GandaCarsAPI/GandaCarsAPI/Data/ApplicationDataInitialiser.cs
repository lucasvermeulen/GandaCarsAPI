﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GandaCarsAPI.Models;
using Microsoft.AspNetCore.Identity;

namespace GandaCarsAPI.Data {
    public class ApplicationDataInitialiser {

        private readonly ApplicationDbContext _dbContext;

        public ApplicationDataInitialiser(ApplicationDbContext dbContext) {
            _dbContext = dbContext;
        }

        public void InitializeData() {
            _dbContext.Database.EnsureDeleted();
            if (_dbContext.Database.EnsureCreated()) {
                BusChauffeur bc = new BusChauffeur() {
                    Voornaam = "Tom",
                    Achternaam = "De Bakker",
                    Uurloon = 11,
                    GeboorteDatum = DateTime.Now,
                    Straatnaam = "pinksterbloemstraat",
                    Huisnummer = "19",
                    Postcode = "9030",
                    Stad = "Mariakerke",
                    Land = "België",
                    Stelsel = 1,
                    AfstandKM = 5.4,
                    OverUren = 360000000,
                    AantalDagenJaarlijksVerlof = 24
                };
                _dbContext.BusChauffeurs.Add(bc);

                BusChauffeur bc1 = new BusChauffeur() {
                    Voornaam = "Brombeer",
                    Achternaam = "Bram",
                    Uurloon = 11,
                    GeboorteDatum = DateTime.Now,
                    Straatnaam = "pinksterbloemstraat",
                    Huisnummer = "19",
                    Postcode = "9030",
                    Stad = "Mariakerke",
                    Land = "België",
                    Stelsel = 1,
                    AfstandKM = 3.4,
                    OverUren = -36000000,
                    AantalDagenJaarlijksVerlof = 24
                };
                _dbContext.BusChauffeurs.Add(bc1);

                BusChauffeur bc2 = new BusChauffeur() {
                    Voornaam = "vogel",
                    Achternaam = "vlinder",
                    Uurloon = 12,
                    GeboorteDatum = DateTime.Now,
                    Straatnaam = "pinksterbloemstraat",
                    Huisnummer = "19",
                    Postcode = "9030",
                    Stad = "Mariakerke",
                    Land = "België",
                    Stelsel = 4,
                    AfstandKM = 2.6,
                    OverUren = -50000000,
                    AantalDagenJaarlijksVerlof = 24
                };
                _dbContext.BusChauffeurs.Add(bc2);
                _dbContext.SaveChanges();
                Dienst d1 = new Dienst() {
                    Naam = "2101",
                    StartUur = new TimeSpan(04, 9, 0),
                    EindUur = new TimeSpan(17, 45, 0),
                    StartDag = 2,
                    EindDag = 2,
                    BusChauffeur = bc

                };

                Onderbreking d1s1 = new Onderbreking() {
                    StartUur = new TimeSpan(5, 13, 0),
                    EindUur = new TimeSpan(5, 35, 0),
                    StartDag = 2. GetHashCode(),
                    EindDag = 2,
                    Type = OnderbrekingsType.Stationnement
                };
                d1.Onderbrekingen.Add(d1s1);

                Onderbreking d1s2 = new Onderbreking() {
                    StartUur = new TimeSpan(7, 24, 0),
                    EindUur = new TimeSpan(7, 40, 0),
                    StartDag = 2,
                    EindDag = 2,
                    Type = OnderbrekingsType.Stationnement
                };
                d1.Onderbrekingen.Add(d1s2);

                Onderbreking d1o1 = new Onderbreking() {
                    StartUur = new TimeSpan(8, 51, 0),
                    EindUur = new TimeSpan(15, 29, 0),
                    StartDag = 2,
                    EindDag = 2,
                    Type = OnderbrekingsType.Onderbreking
                };
                d1.Onderbrekingen.Add(d1o1);

                Onderbreking d1s3 = new Onderbreking() {
                    StartUur = new TimeSpan(16, 25, 0),
                    EindUur = new TimeSpan(16, 29, 0),
                    StartDag = 2,
                    EindDag = 2,
                    Type = OnderbrekingsType.Stationnement
                };
                d1.Onderbrekingen.Add(d1s3);
                bc.Diensten.Add(d1);

                Dienst d2 = new Dienst() {
                    Naam = "2102",
                    StartUur = new TimeSpan(6, 39, 0),
                    EindUur = new TimeSpan(17, 37, 0),
                    StartDag = 3,
                    EindDag = 3,
                    BusChauffeur = bc
                };

                Onderbreking d2s1 = new Onderbreking() {
                    StartUur = new TimeSpan(7, 53, 0),
                    EindUur = new TimeSpan(7, 56, 0),
                    StartDag = 3,
                    EindDag = 3,
                    Type = OnderbrekingsType.Stationnement
                };
                d2.Onderbrekingen.Add(d2s1);

                Onderbreking d2o1 = new Onderbreking() {
                    StartUur = new TimeSpan(8, 33, 0),
                    EindUur = new TimeSpan(10, 51, 0),
                    StartDag = 3,
                    EindDag = 3,
                    Type = OnderbrekingsType.Onderbreking
                };
                d2.Onderbrekingen.Add(d2o1);

                Onderbreking d2s2 = new Onderbreking() {
                    StartUur = new TimeSpan(12, 20, 0),
                    EindUur = new TimeSpan(12, 45, 0),
                    StartDag = 3,
                    EindDag = 3,
                    Type = OnderbrekingsType.Stationnement
                };
                d2.Onderbrekingen.Add(d2s2);

                Onderbreking d2s3 = new Onderbreking() {
                    StartUur = new TimeSpan(14, 16, 0),
                    EindUur = new TimeSpan(15, 44, 0),
                    StartDag = 3,
                    EindDag = 3,
                    Type = OnderbrekingsType.Stationnement
                };
                d2.Onderbrekingen.Add(d2s3);

                Onderbreking d2s4 = new Onderbreking() {
                    StartUur = new TimeSpan(16, 17, 0),
                    EindUur = new TimeSpan(16, 19, 0),
                    StartDag = 3,
                    EindDag = 3,
                    Type = OnderbrekingsType.Stationnement
                };
                d2.Onderbrekingen.Add(d2s4);
                bc.Diensten.Add(d2);

                Dienst d3 = new Dienst() {
                    Naam = "2103",
                    StartUur = new TimeSpan(6, 12, 0),
                    EindUur = new TimeSpan(15, 46, 0),
                    StartDag = 4,
                    EindDag = 4,
                    BusChauffeur = bc
                };

                Onderbreking d3s1 = new Onderbreking() {
                    StartUur = new TimeSpan(7, 20, 0),
                    EindUur = new TimeSpan(7, 26, 0),
                    StartDag = 4,
                    EindDag = 4,
                    Type = OnderbrekingsType.Stationnement
                };
                d3.Onderbrekingen.Add(d3s1);

                Onderbreking d3s2 = new Onderbreking() {
                    StartUur = new TimeSpan(8, 22, 0),
                    EindUur = new TimeSpan(8, 26, 0),
                    StartDag = 4,
                    EindDag = 4,
                    Type = OnderbrekingsType.Stationnement
                };
                d3.Onderbrekingen.Add(d3s2);

                Onderbreking d3s3 = new Onderbreking() {
                    StartUur = new TimeSpan(9, 20, 0),
                    EindUur = new TimeSpan(9, 29, 0),
                    StartDag = 4,
                    EindDag = 4,
                    Type = OnderbrekingsType.Stationnement
                };
                d3.Onderbrekingen.Add(d3s3);

                Onderbreking d3s4 = new Onderbreking() {
                    StartUur = new TimeSpan(10, 19, 0),
                    EindUur = new TimeSpan(10, 44, 0),
                    StartDag = 4,
                    EindDag = 4,
                    Type = OnderbrekingsType.Stationnement
                };
                d3.Onderbrekingen.Add(d3s4);

                Onderbreking d3s5 = new Onderbreking() {
                    StartUur = new TimeSpan(11, 34, 0),
                    EindUur = new TimeSpan(11, 44, 0),
                    StartDag = 4,
                    EindDag = 4,
                    Type = OnderbrekingsType.Stationnement
                };
                d3.Onderbrekingen.Add(d3s5);
                Onderbreking d3s6 = new Onderbreking() {
                    StartUur = new TimeSpan(12, 34, 0),
                    EindUur = new TimeSpan(12, 44, 0),
                    StartDag = 4,
                    EindDag = 4,
                    Type = OnderbrekingsType.Stationnement
                };
                d3.Onderbrekingen.Add(d3s6);
                Onderbreking d3s7 = new Onderbreking() {
                    StartUur = new TimeSpan(13, 34, 0),
                    EindUur = new TimeSpan(13, 44, 0),
                    StartDag = 4,
                    EindDag = 4,
                    Type = OnderbrekingsType.Stationnement
                };
                d3.Onderbrekingen.Add(d3s7);
                Onderbreking d3s8 = new Onderbreking() {
                    StartUur = new TimeSpan(14, 34, 0),
                    EindUur = new TimeSpan(14, 59, 0),
                    StartDag = 4,
                    EindDag = 4,
                    Type = OnderbrekingsType.Stationnement
                };
                d3.Onderbrekingen.Add(d3s8);
                bc.Diensten.Add(d3);

                Dienst d4 = new Dienst() {
                    Naam = "2104",
                    StartUur = new TimeSpan(6, 41, 0),
                    EindUur = new TimeSpan(18, 02, 0),
                    StartDag = 5,
                    EindDag = 5,
                    BusChauffeur = bc
                };

                Onderbreking d4o1 = new Onderbreking() {
                    StartUur = new TimeSpan(8, 11, 0),
                    EindUur = new TimeSpan(14, 47, 0),
                    StartDag = 5,
                    EindDag = 5,
                    Type = OnderbrekingsType.Onderbreking
                };
                d4.Onderbrekingen.Add(d4o1);

                Onderbreking d4s1 = new Onderbreking() {
                    StartUur = new TimeSpan(16, 10, 0),
                    EindUur = new TimeSpan(16, 12, 0),
                    StartDag = 5,
                    EindDag = 5,
                    Type = OnderbrekingsType.Stationnement
                };
                d4.Onderbrekingen.Add(d4s1);

                Onderbreking d4s2 = new Onderbreking() {
                    StartUur = new TimeSpan(16, 47, 0),
                    EindUur = new TimeSpan(16, 50, 0),
                    StartDag = 5,
                    EindDag = 5,
                    Type = OnderbrekingsType.Stationnement
                };
                d4.Onderbrekingen.Add(d4s2);

                bc.Diensten.Add(d4);
                Dienst d5 = new Dienst() {
                    Naam = "2105",
                    StartUur = new TimeSpan(6, 45, 0),
                    EindUur = new TimeSpan(18, 25, 0),
                    StartDag = 1,
                    EindDag = 1,
                    BusChauffeur = bc
                };

                Onderbreking d5o1 = new Onderbreking() {
                    StartUur = new TimeSpan(8, 12, 0),
                    EindUur = new TimeSpan(11, 36, 0),
                    StartDag = 1,
                    EindDag = 1,
                    Type = OnderbrekingsType.Onderbreking
                };
                d5.Onderbrekingen.Add(d5o1);

                Onderbreking d5s1 = new Onderbreking() {
                    StartUur = new TimeSpan(13, 49, 0),
                    EindUur = new TimeSpan(14, 08, 0),
                    StartDag = 1,
                    EindDag = 1,
                    Type = OnderbrekingsType.Stationnement
                };
                d5.Onderbrekingen.Add(d5s1);

                Onderbreking d5s2 = new Onderbreking() {
                    StartUur = new TimeSpan(16, 07, 0),
                    EindUur = new TimeSpan(16, 35, 0),
                    StartDag = 1,
                    EindDag = 1,
                    Type = OnderbrekingsType.Stationnement
                };
                d5.Onderbrekingen.Add(d5s2);
                bc.Diensten.Add(d5);
                Dienst d6 = new Dienst() {
                    Naam = "2106",
                    StartUur = new TimeSpan(6, 46, 0),
                    EindUur = new TimeSpan(18, 03, 0),
                    StartDag = 2,
                    EindDag = 2,
                    BusChauffeur = bc
                };

                Onderbreking d6s1 = new Onderbreking() {
                    StartUur = new TimeSpan(7, 56, 0),
                    EindUur = new TimeSpan(8, 00, 0),
                    StartDag = 2,
                    EindDag = 2,
                    Type = OnderbrekingsType.Stationnement
                };
                d6.Onderbrekingen.Add(d6s1);

                Onderbreking d6o1 = new Onderbreking() {
                    StartUur = new TimeSpan(8, 57, 0),
                    EindUur = new TimeSpan(15, 37, 0),
                    StartDag = 2,
                    EindDag = 2,
                    Type = OnderbrekingsType.Onderbreking
                };
                d6.Onderbrekingen.Add(d6o1);

                Onderbreking d6s2 = new Onderbreking() {
                    StartUur = new TimeSpan(16, 22, 0),
                    EindUur = new TimeSpan(16, 35, 0),
                    StartDag = 2,
                    EindDag = 2,
                    Type = OnderbrekingsType.Stationnement
                };
                d6.Onderbrekingen.Add(d6s2);

                bc.Diensten.Add(d6);

                Dienst d7 = new Dienst() {
                    Naam = "2107",
                    StartUur = new TimeSpan(7, 10, 0),
                    EindUur = new TimeSpan(18, 39, 0),
                    StartDag = 3,
                    EindDag = 3,
                    BusChauffeur = bc
                };

                Onderbreking d7s1 = new Onderbreking() {
                    StartUur = new TimeSpan(7, 51, 0),
                    EindUur = new TimeSpan(8, 01, 0),
                    StartDag = 3,
                    EindDag = 3,
                    Type = OnderbrekingsType.Stationnement
                };
                d7.Onderbrekingen.Add(d7s1);

                Onderbreking d7o1 = new Onderbreking() {
                    StartUur = new TimeSpan(8, 42, 0),
                    EindUur = new TimeSpan(15, 29, 0),
                    StartDag = 3,
                    EindDag = 3,
                    Type = OnderbrekingsType.Onderbreking
                };
                d7.Onderbrekingen.Add(d7o1);

                Onderbreking d7s2 = new Onderbreking() {
                    StartUur = new TimeSpan(16, 21, 0),
                    EindUur = new TimeSpan(16, 24, 0),
                    StartDag = 3,
                    EindDag = 3,
                    Type = OnderbrekingsType.Stationnement
                };
                d7.Onderbrekingen.Add(d7s2);

                Onderbreking d7s3 = new Onderbreking() {
                    StartUur = new TimeSpan(16, 53, 0),
                    EindUur = new TimeSpan(17, 10, 0),
                    StartDag = 3,
                    EindDag = 3,
                    Type = OnderbrekingsType.Stationnement
                };
                d7.Onderbrekingen.Add(d7s3);

                bc.Diensten.Add(d7);
                Dienst d8 = new Dienst() {
                    Naam = "2108",
                    StartUur = new TimeSpan(7, 11, 0),
                    EindUur = new TimeSpan(23, 36, 0),
                    StartDag = 4,
                    EindDag = 4,
                    BusChauffeur = bc
                };

                Onderbreking d8o1 = new Onderbreking() {
                    StartUur = new TimeSpan(8, 22, 0),
                    EindUur = new TimeSpan(16, 11, 0),
                    StartDag = 4,
                    EindDag = 4,
                    Type = OnderbrekingsType.Onderbreking
                };
                d8.Onderbrekingen.Add(d8o1);

                Onderbreking d8s1 = new Onderbreking() {
                    StartUur = new TimeSpan(16, 55, 0),
                    EindUur = new TimeSpan(17, 07, 0),
                    StartDag = 4,
                    EindDag = 4,
                    Type = OnderbrekingsType.Stationnement
                };
                d8.Onderbrekingen.Add(d8s1);

                Onderbreking d8s2 = new Onderbreking() {
                    StartUur = new TimeSpan(18, 56, 0),
                    EindUur = new TimeSpan(19, 31, 0),
                    StartDag = 4,
                    EindDag = 4,
                    Type = OnderbrekingsType.Stationnement
                };
                d8.Onderbrekingen.Add(d8s2);

                Onderbreking d8s3 = new Onderbreking() {
                    StartUur = new TimeSpan(21, 33, 0),
                    EindUur = new TimeSpan(21, 39, 0),
                    StartDag = 4,
                    EindDag = 4,
                    Type = OnderbrekingsType.Stationnement
                };
                d8.Onderbrekingen.Add(d8s3);

                bc.Diensten.Add(d8);

                Dienst d9 = new Dienst() {
                    Naam = "2601",
                    StartUur = new TimeSpan(6, 14, 0),
                    EindUur = new TimeSpan(14, 04, 0),
                    StartDag = 6,
                    EindDag = 6,
                    BusChauffeur = bc
                };

                Onderbreking d9s1 = new Onderbreking() {
                    StartUur = new TimeSpan(7, 17, 0),
                    EindUur = new TimeSpan(7, 31, 0),
                    StartDag = 6,
                    EindDag = 6,
                    Type = OnderbrekingsType.Stationnement
                };
                d9.Onderbrekingen.Add(d9s1);

                Onderbreking d9s2 = new Onderbreking() {
                    StartUur = new TimeSpan(8, 17, 0),
                    EindUur = new TimeSpan(8, 31, 0),
                    StartDag = 6,
                    EindDag = 6,
                    Type = OnderbrekingsType.Stationnement
                };
                d9.Onderbrekingen.Add(d9s2);

                Onderbreking d9s3 = new Onderbreking() {
                    StartUur = new TimeSpan(9, 17, 0),
                    EindUur = new TimeSpan(10, 01, 0),
                    StartDag = 6,
                    EindDag = 6,
                    Type = OnderbrekingsType.Stationnement
                };
                d9.Onderbrekingen.Add(d9s3);

                Onderbreking d9s4 = new Onderbreking() {
                    StartUur = new TimeSpan(10, 47, 0),
                    EindUur = new TimeSpan(11, 01, 0),
                    StartDag = 6,
                    EindDag = 6,
                    Type = OnderbrekingsType.Stationnement
                };
                d9.Onderbrekingen.Add(d9s4);

                Onderbreking d9s5 = new Onderbreking() {
                    StartUur = new TimeSpan(11, 47, 0),
                    EindUur = new TimeSpan(12, 01, 0),
                    StartDag = 6,
                    EindDag = 6,
                    Type = OnderbrekingsType.Stationnement
                };
                d9.Onderbrekingen.Add(d9s5);

                Onderbreking d9s6 = new Onderbreking() {
                    StartUur = new TimeSpan(12, 47, 0),
                    EindUur = new TimeSpan(13, 00, 0),
                    StartDag = 6,
                    EindDag = 6,
                    Type = OnderbrekingsType.Stationnement
                };
                d9.Onderbrekingen.Add(d9s6);

                bc.Diensten.Add(d9);

                Dienst d10 = new Dienst() {
                    Naam = "2120",
                    StartUur = new TimeSpan(4, 35, 0),
                    EindUur = new TimeSpan(13, 25, 0),
                    StartDag = 5,
                    EindDag = 5,
                    BusChauffeur = bc
                };

                Onderbreking d10s1 = new Onderbreking() {
                    StartUur = new TimeSpan(5, 49, 0),
                    EindUur = new TimeSpan(6, 09, 0),
                    StartDag = 5,
                    EindDag = 5,
                    Type = OnderbrekingsType.Stationnement
                };
                d10.Onderbrekingen.Add(d10s1);

                Onderbreking d10s2 = new Onderbreking() {
                    StartUur = new TimeSpan(7, 02, 0),
                    EindUur = new TimeSpan(7, 22, 0),
                    StartDag = 5,
                    EindDag = 5,
                    Type = OnderbrekingsType.Stationnement
                };
                d10.Onderbrekingen.Add(d10s2);

                Onderbreking d10s3 = new Onderbreking() {
                    StartUur = new TimeSpan(8, 40, 0),
                    EindUur = new TimeSpan(8, 56, 0),
                    StartDag = 5,
                    EindDag = 5,
                    Type = OnderbrekingsType.Stationnement
                };
                d10.Onderbrekingen.Add(d10s3);

                Onderbreking d10s4 = new Onderbreking() {
                    StartUur = new TimeSpan(10, 49, 0),
                    EindUur = new TimeSpan(11, 08, 0),
                    StartDag = 5,
                    EindDag = 5,
                    Type = OnderbrekingsType.Stationnement
                };
                d10.Onderbrekingen.Add(d10s4);

                bc.Diensten.Add(d10);

                Dienst d11 = new Dienst() {
                    Naam = "2620",
                    StartUur = new TimeSpan(5, 40, 0),
                    EindUur = new TimeSpan(15, 49, 0),
                    StartDag = 6,
                    EindDag = 6,
                    BusChauffeur = bc
                };

                Onderbreking d11s1 = new Onderbreking() {
                    StartUur = new TimeSpan(6, 57, 0),
                    EindUur = new TimeSpan(7, 08, 0),
                    StartDag = 6,
                    EindDag = 6,
                    Type = OnderbrekingsType.Stationnement
                };
                d11.Onderbrekingen.Add(d11s1);

                Onderbreking d11s2 = new Onderbreking() {
                    StartUur = new TimeSpan(8, 56, 0),
                    EindUur = new TimeSpan(9, 14, 0),
                    StartDag = 6,
                    EindDag = 6,
                    Type = OnderbrekingsType.Stationnement
                };
                d11.Onderbrekingen.Add(d11s2);

                Onderbreking d11s3 = new Onderbreking() {
                    StartUur = new TimeSpan(11, 04, 0),
                    EindUur = new TimeSpan(11, 56, 0),
                    StartDag = 6,
                    EindDag = 6,
                    Type = OnderbrekingsType.Stationnement
                };
                d11.Onderbrekingen.Add(d11s3);

                Onderbreking d11s4 = new Onderbreking() {
                    StartUur = new TimeSpan(13, 49, 0),
                    EindUur = new TimeSpan(13, 55, 0),
                    StartDag = 6,
                    EindDag = 6,
                    Type = OnderbrekingsType.Stationnement
                };
                d11.Onderbrekingen.Add(d11s4);

                bc.Diensten.Add(d11);

                Dienst d12 = new Dienst() {
                    Naam = "2701",
                    StartUur = new TimeSpan(6, 14, 0),
                    EindUur = new TimeSpan(16, 17, 0),
                    StartDag = 7,
                    EindDag = 7,
                    BusChauffeur = bc
                };

                Onderbreking d12s1 = new Onderbreking() {
                    StartUur = new TimeSpan(7, 16, 0),
                    EindUur = new TimeSpan(7, 31, 0),
                    StartDag = 7,
                    EindDag = 7,
                    Type = OnderbrekingsType.Stationnement
                };
                d12.Onderbrekingen.Add(d12s1);

                Onderbreking d12s2 = new Onderbreking() {
                    StartUur = new TimeSpan(8, 18, 0),
                    EindUur = new TimeSpan(8, 31, 0),
                    StartDag = 7,
                    EindDag = 7,
                    Type = OnderbrekingsType.Stationnement
                };
                d12.Onderbrekingen.Add(d12s2);

                Onderbreking d12s3 = new Onderbreking() {
                    StartUur = new TimeSpan(9, 18, 0),
                    EindUur = new TimeSpan(9, 31, 0),
                    StartDag = 7,
                    EindDag = 7,
                    Type = OnderbrekingsType.Stationnement
                };
                d12.Onderbrekingen.Add(d12s3);

                Onderbreking d12s4 = new Onderbreking() {
                    StartUur = new TimeSpan(10, 18, 0),
                    EindUur = new TimeSpan(10, 31, 0),
                    StartDag = 7,
                    EindDag = 7,
                    Type = OnderbrekingsType.Stationnement
                };
                d12.Onderbrekingen.Add(d12s4);

                Onderbreking d12s5 = new Onderbreking() {
                    StartUur = new TimeSpan(11, 18, 0),
                    EindUur = new TimeSpan(12, 0, 0),
                    StartDag = 7,
                    EindDag = 7,
                    Type = OnderbrekingsType.Stationnement
                };
                d12.Onderbrekingen.Add(d12s5);

                Onderbreking d12s6 = new Onderbreking() {
                    StartUur = new TimeSpan(12, 49, 0),
                    EindUur = new TimeSpan(13, 0, 0),
                    StartDag = 7,
                    EindDag = 7,
                    Type = OnderbrekingsType.Stationnement
                };
                d12.Onderbrekingen.Add(d12s6);

                Onderbreking d12s7 = new Onderbreking() {
                    StartUur = new TimeSpan(13, 49, 0),
                    EindUur = new TimeSpan(14, 30, 0),
                    StartDag = 7,
                    EindDag = 7,
                    Type = OnderbrekingsType.Stationnement
                };
                d12.Onderbrekingen.Add(d12s7);

                Onderbreking d12s8 = new Onderbreking() {
                    StartUur = new TimeSpan(15, 19, 0),
                    EindUur = new TimeSpan(15, 30, 0),
                    StartDag = 7,
                    EindDag = 7,
                    Type = OnderbrekingsType.Stationnement
                };
                d12.Onderbrekingen.Add(d12s8);

                bc.Diensten.Add(d12);

                _dbContext.SaveChanges();

            }
        }

    }
}