﻿using System;
using System.Collections.Generic;
using System.Linq;
using GandaCarsAPI.DTO;
using GandaCarsAPI.Models;
using GandaCarsAPI.Models.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace GandaCarsAPI.Data.Repositories {
    public class LoonficheSettingsRepository : ILoonficheSettingsRepository {
        private readonly ApplicationDbContext _context;
        private readonly DbSet<LoonficheSetting> _loonficheSettings;

        public LoonficheSettingsRepository(ApplicationDbContext dbContext) {
            _context = dbContext;
            _loonficheSettings = dbContext.LoonficheSettings;
        }

        public LoonficheSetting Add(LoonficheSetting lfs) {
            _loonficheSettings.Add(lfs);
            return lfs;
        }

        public LoonficheSetting Delete(LoonficheSetting lfs) {
            _loonficheSettings.Remove(lfs);
            return lfs;
        }

        public IEnumerable<LoonficheSetting> GetAll() {
            return _loonficheSettings.ToList();
        }

        public LoonficheSetting GetById(string id) {
            return _loonficheSettings.Include(x => x.BusChauffeur).FirstOrDefault(x => x.Id == id);
        }

        public LoonficheSetting GetByYearAndMonth(BusChauffeur bc, int jaar, int maand) {
            return _loonficheSettings.FirstOrDefault(x => x.BusChauffeur == bc && x.Jaar == jaar && x.Maand == maand);
        }

        public void SaveChanges() {
            _context.SaveChanges();
        }
        public LoonficheSetting Update(LoonficheSetting lfs) {
            _loonficheSettings.Update(lfs);
            return lfs;
        }
    }
}