﻿using System;
using System.Collections.Generic;
using System.Linq;
using GandaCarsAPI.Models;
using GandaCarsAPI.Models.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace GandaCarsAPI.Data.Repositories {
    public class DienstRepository : IDienstRepository {
        private readonly ApplicationDbContext _context;
        private readonly DbSet<Dienst> _dienst;

        public DienstRepository(ApplicationDbContext dbContext) {
            _context = dbContext;
            _dienst = dbContext.Diensten;
        }

        public List<Dienst> TryGetDienstByName(string name) {
            return _dienst.Include(t => t.BusChauffeur).Include(t => t.Onderbrekingen).ToList().FindAll(x => x.Naam.Contains(name));
        }

        public void Add(Dienst dienst) {
            _dienst.Add(dienst);
        }

        public void Delete(Dienst dienst) {
            _dienst.Remove(dienst);
        }

        public IEnumerable<Dienst> GetAll() {
            return _dienst.OrderBy(t => t.DagVanToevoegen).Include(t => t.BusChauffeur).Include(t => t.Onderbrekingen).ToList();
        }

        public List<Dienst> GetAllVan(BusChauffeur bc) {
            return _dienst.OrderBy(t => t.DagVanToevoegen).Where(t => t.BusChauffeur == bc).Include(t => t.BusChauffeur).Include(t => t.Onderbrekingen).ToList();
        }

        public Dienst GetBy(string id) {
            return _dienst.Include(t => t.BusChauffeur).Include(t => t.Onderbrekingen).SingleOrDefault(r => r.Id == id);
        }

        public Dienst GetByName(string naam) {
            return _dienst.Include(t => t.BusChauffeur).Include(t => t.Onderbrekingen).SingleOrDefault(r => r.Naam == naam);
        }
        public void SaveChanges() {
            _context.SaveChanges();
        }

        public void Update(Dienst dienst) {
            _dienst.Update(dienst);
        }
    }
}