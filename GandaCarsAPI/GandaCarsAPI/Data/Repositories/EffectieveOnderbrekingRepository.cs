﻿using System;
using System.Collections.Generic;
using System.Linq;
using GandaCarsAPI.Models;
using GandaCarsAPI.Models.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace GandaCarsAPI.Data.Repositories
{
    public class EffectieveOnderbrekingRepository : IEffectieveOnderbrekingRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly DbSet<EffectieveOnderbreking> _effectieveOnderbrekingen;

        public EffectieveOnderbrekingRepository(ApplicationDbContext dbContext)
        {
            _context = dbContext;
            _effectieveOnderbrekingen = dbContext.EffectieveOnderbrekingen;
        }

        public void Add(EffectieveOnderbreking ond)
        {
            _effectieveOnderbrekingen.Add(ond);
        }

        public void AddRange(List<EffectieveOnderbreking> ond)
        {
            _effectieveOnderbrekingen.AddRange(ond);
        }

        public void Delete(EffectieveOnderbreking ond)
        {
            _effectieveOnderbrekingen.Remove(ond);
        }

        public void DeleteRange(List<EffectieveOnderbreking> ond)
        {
            _effectieveOnderbrekingen.RemoveRange(ond);
        }

        public IEnumerable<EffectieveOnderbreking> GetAll()
        {
            return _effectieveOnderbrekingen.ToList();
        }

        public EffectieveOnderbreking GetBy(string id)
        {
            return _effectieveOnderbrekingen.FirstOrDefault(t => t.Id == id);
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void Update(EffectieveOnderbreking ond)
        {
            _context.Update(ond);
        }
    }
}
