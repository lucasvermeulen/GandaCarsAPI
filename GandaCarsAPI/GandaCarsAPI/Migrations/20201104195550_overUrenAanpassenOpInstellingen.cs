﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GandaCarsAPI.Migrations
{
    public partial class overUrenAanpassenOpInstellingen : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "OverurenAanpassen",
                table: "Instellingen",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OverurenAanpassen",
                table: "Instellingen");
        }
    }
}
