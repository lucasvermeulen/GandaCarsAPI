﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GandaCarsAPI.Migrations {
    public partial class rmEmailAddAdress : Migration {
        protected override void Up(MigrationBuilder migrationBuilder) {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "BusChauffeurs");
            migrationBuilder.AddColumn<string>(
                name: "Huisnummer",
                table: "BusChauffeurs",
                nullable : true);

            migrationBuilder.AddColumn<string>(
                name: "Land",
                table: "BusChauffeurs",
                nullable : true);

            migrationBuilder.AddColumn<string>(
                name: "Postcode",
                table: "BusChauffeurs",
                nullable : true);

            migrationBuilder.AddColumn<string>(
                name: "Stad",
                table: "BusChauffeurs",
                nullable : true);
            migrationBuilder.AddColumn<string>(
                name: "Straatnaam",
                table: "BusChauffeurs",
                nullable : true);
        }

        protected override void Down(MigrationBuilder migrationBuilder) {
            migrationBuilder.DropColumn(
                name: "Huisnummer",
                table: "BusChauffeurs");

            migrationBuilder.DropColumn(
                name: "Land",
                table: "BusChauffeurs");

            migrationBuilder.DropColumn(
                name: "Postcode",
                table: "BusChauffeurs");

            migrationBuilder.DropColumn(
                name: "Stad",
                table: "BusChauffeurs");

            migrationBuilder.DropColumn(
                name: "Straatnaam",
                table: "BusChauffeurs");

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "BusChauffeurs",
                nullable : true);
        }
    }
}