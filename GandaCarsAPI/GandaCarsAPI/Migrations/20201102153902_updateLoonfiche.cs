﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GandaCarsAPI.Migrations {
    public partial class updateLoonfiche : Migration {
        protected override void Up(MigrationBuilder migrationBuilder) {
            migrationBuilder.RenameColumn(
                name: "type",
                table: "Onderbrekingen",
                newName: "Type");

            migrationBuilder.RenameColumn(
                name: "type",
                table: "EffectieveOnderbrekingen",
                newName: "Type");

            migrationBuilder.AlterColumn<TimeSpan>(
                name: "StartUur",
                table: "Onderbrekingen",
                nullable : false,
                oldClrType : typeof(DateTimeOffset));
            migrationBuilder.Sql("update Onderbrekingen set StartUur = cast(concat(cast(cast( FORMAT (StartUur,'hh')as int)+1 as varchar),':', cast(FORMAT (StartUur,'mm')as varchar)) as Time)");

            migrationBuilder.AlterColumn<TimeSpan>(
                name: "EindUur",
                table: "Onderbrekingen",
                nullable : false,
                oldClrType : typeof(DateTimeOffset));
            migrationBuilder.Sql("update Onderbrekingen set EindUur = cast(concat(cast(cast( FORMAT (EindUur,'hh')as int)+1 as varchar),':', cast(FORMAT (EindUur,'mm')as varchar)) as Time)");

            migrationBuilder.AddColumn<double>(
                name: "AantalMinutenAdministratieveTijdVoorDienstMetMeerDan2Onderbrekingen",
                table: "Instellingen",
                nullable : false,
                defaultValue : 5400000);

            migrationBuilder.AddColumn<double>(
                name: "AantalUrenStandaardWerkdag",
                table: "Instellingen",
                nullable : false,
                defaultValue : 22200000);

            migrationBuilder.AddColumn<string>(
                name: "DefaultGeselecteerdeKolommenBijBuschauffeurTabel",
                table: "Instellingen",
                nullable : false,
                defaultValue: "[{\"field\":\"voornaam\",\"header\":\"Voornaam\"},{\"field\":\"achternaam\",\"header\":\"Achternaam\"},{\"field\":\"uurloon\",\"header\":\"Uurloon\"},{\"field\":\"geboorteDatum\",\"header\":\"Geboortedatum\"},{\"field\":\"afstandKM\",\"header\":\"Afstand(KM)\"},{\"field\":\"stelsel.id\",\"header\":\"Stelsel\"},{\"field\":\"overUren\",\"header\":\"Overuren\"},{\"field\":\"aantalDagenJaarlijksVerlof\",\"header\":\"AantaldagenJV\"}]"
            );

            migrationBuilder.AddColumn<double>(
                name: "DefaultVoorschot1",
                table: "Instellingen",
                nullable : false,
                defaultValue : 550);

            migrationBuilder.AddColumn<double>(
                name: "DefaultVoorschot2",
                table: "Instellingen",
                nullable : false,
                defaultValue : 550);

            migrationBuilder.AddColumn<double>(
                name: "DefaultVoorschotVolgendeMaand",
                table: "Instellingen",
                nullable : false,
                defaultValue : 550);

            migrationBuilder.AddColumn<double>(
                name: "GewaarbordLoonLaatsteDeel",
                table: "Instellingen",
                nullable : false,
                defaultValue : 25.88);

            migrationBuilder.AddColumn<double>(
                name: "GewaarborgdLoonEersteWeek",
                table: "Instellingen",
                nullable : false,
                defaultValue : 100);

            migrationBuilder.AddColumn<double>(
                name: "GewaarborgdLoonTweedeWeek",
                table: "Instellingen",
                nullable : false,
                defaultValue : 85.88);

            migrationBuilder.AddColumn<double>(
                name: "WerktijdFeestdag",
                table: "Instellingen",
                nullable : false,
                defaultValue : 22200000);

            migrationBuilder.AlterColumn<int>(
                name: "AndereMinuten",
                table: "EffectieveDiensten",
                nullable : true,
                oldClrType : typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "EffectieveDiensten",
                nullable : false,
                defaultValue : 1);

            migrationBuilder.AlterColumn<TimeSpan>(
                name: "StartUur",
                table: "Diensten",
                nullable : true,
                oldClrType : typeof(DateTimeOffset));
            migrationBuilder.Sql("update Diensten set StartUur = cast(concat(cast(cast( FORMAT (StartUur,'hh')as int)+1 as varchar),':', cast(FORMAT (StartUur,'mm')as varchar)) as Time)");

            migrationBuilder.AlterColumn<TimeSpan>(
                name: "EindUur",
                table: "Diensten",
                nullable : true,
                oldClrType : typeof(DateTimeOffset));
            migrationBuilder.Sql("update Diensten set EindUur = cast(concat(cast(cast( FORMAT (EindUur,'hh')as int)+1 as varchar),':', cast(FORMAT (EindUur,'mm')as varchar)) as Time)");

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "Diensten",
                nullable : false,
                defaultValue : 1);

            migrationBuilder.AlterColumn<double>(
                name: "Uurloon",
                table: "BusChauffeurs",
                nullable : false,
                oldClrType : typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "AantalDagenJaarlijksVerlof",
                table: "BusChauffeurs",
                nullable : false,
                defaultValue : 24);

            migrationBuilder.AddColumn<double>(
                name: "AfstandKM",
                table: "BusChauffeurs",
                nullable : false,
                defaultValue : 0.0);

            migrationBuilder.AddColumn<double>(
                name: "OverUren",
                table: "BusChauffeurs",
                nullable : false,
                defaultValue : 0.0);

            migrationBuilder.CreateTable(
                name: "LoonficheSettings",
                columns : table => new {
                    Id = table.Column<string>(nullable: false),
                        BusChauffeurId = table.Column<string>(nullable: true),
                        Jaar = table.Column<int>(nullable: false),
                        Maand = table.Column<int>(nullable: false),
                        MilisecondenGewaarborgdLoonEersteWeek = table.Column<double>(nullable: false),
                        MilisecondenGewaarborgdLoonTweedeWeek = table.Column<double>(nullable: false),
                        MilisecondenGewaarborgdLoonLaatsteDeel = table.Column<double>(nullable: false),
                        RSZ = table.Column<double>(nullable: false),
                        Bedrijsvoorheffing = table.Column<double>(nullable: false),
                        FietsRitten = table.Column<int>(nullable: false),
                        ARABvergoeding = table.Column<double>(nullable: false),
                        VoorschotVolgendeMaand = table.Column<double>(nullable: false),
                        OntvangenVoorschot1 = table.Column<double>(nullable: false),
                        OntvangenVoorschot2 = table.Column<double>(nullable: false),
                        OverurenOpnemen = table.Column<double>(nullable: false),
                        NieuweOverUrenBc = table.Column<double>(nullable: false),
                        ResterendeUrenDezeMaand = table.Column<double>(nullable: false),
                        Loonbeslag = table.Column<double>(nullable: false),
                        GewerkteUrenDezeMaand = table.Column<double>(nullable: false)
                },
                constraints : table => {
                    table.PrimaryKey("PK_LoonficheSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LoonficheSettings_BusChauffeurs_BusChauffeurId",
                        column : x => x.BusChauffeurId,
                        principalTable: "BusChauffeurs",
                        principalColumn: "Id",
                        onDelete : ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LoonficheSettings_BusChauffeurId",
                table: "LoonficheSettings",
                column: "BusChauffeurId");

            migrationBuilder.Sql("SELECT AantalMinutenAdministratieveTijdVoorDienst FROM Instellingen UPDATE Instellingen SET AantalMinutenAdministratieveTijdVoorDienst = AantalMinutenAdministratieveTijdVoorDienst * 60000;");
        }

        protected override void Down(MigrationBuilder migrationBuilder) {
            migrationBuilder.DropTable(
                name: "LoonficheSettings");

            migrationBuilder.DropColumn(
                name: "AantalMinutenAdministratieveTijdVoorDienstMetMeerDan2Onderbrekingen",
                table: "Instellingen");

            migrationBuilder.DropColumn(
                name: "AantalUrenStandaardWerkdag",
                table: "Instellingen");

            migrationBuilder.DropColumn(
                name: "DefaultGeselecteerdeKolommenBijBuschauffeurTabel",
                table: "Instellingen");

            migrationBuilder.DropColumn(
                name: "DefaultVoorschot1",
                table: "Instellingen");

            migrationBuilder.DropColumn(
                name: "DefaultVoorschot2",
                table: "Instellingen");

            migrationBuilder.DropColumn(
                name: "DefaultVoorschotVolgendeMaand",
                table: "Instellingen");

            migrationBuilder.DropColumn(
                name: "GewaarbordLoonLaatsteDeel",
                table: "Instellingen");

            migrationBuilder.DropColumn(
                name: "GewaarborgdLoonEersteWeek",
                table: "Instellingen");

            migrationBuilder.DropColumn(
                name: "GewaarborgdLoonTweedeWeek",
                table: "Instellingen");

            migrationBuilder.DropColumn(
                name: "WerktijdFeestdag",
                table: "Instellingen");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "EffectieveDiensten");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "Diensten");

            migrationBuilder.DropColumn(
                name: "AantalDagenJaarlijksVerlof",
                table: "BusChauffeurs");

            migrationBuilder.DropColumn(
                name: "AfstandKM",
                table: "BusChauffeurs");

            migrationBuilder.DropColumn(
                name: "OverUren",
                table: "BusChauffeurs");

            migrationBuilder.RenameColumn(
                name: "Type",
                table: "Onderbrekingen",
                newName: "type");

            migrationBuilder.RenameColumn(
                name: "Type",
                table: "EffectieveOnderbrekingen",
                newName: "type");

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "StartUur",
                table: "Onderbrekingen",
                nullable : false,
                oldClrType : typeof(TimeSpan));
            migrationBuilder.Sql("update Onderbrekingen set StartUur = cast(concat(cast(cast( FORMAT (StartUur,'hh')as int)-1 as varchar),':', cast(FORMAT (StartUur,'mm')as varchar)) as Time)");

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "EindUur",
                table: "Onderbrekingen",
                nullable : false,
                oldClrType : typeof(TimeSpan));
            migrationBuilder.Sql("update Onderbrekingen set EindUur = cast(concat(cast(cast( FORMAT (EindUur,'hh')as int)-1 as varchar),':', cast(FORMAT (EindUur,'mm')as varchar)) as Time)");

            migrationBuilder.AlterColumn<int>(
                name: "AndereMinuten",
                table: "EffectieveDiensten",
                nullable : false,
                oldClrType : typeof(int),
                oldNullable : true);

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "StartUur",
                table: "Diensten",
                nullable : false,
                oldClrType : typeof(TimeSpan),
                oldNullable : true);

            migrationBuilder.Sql("update Diensten set StartUur = cast(concat(cast(cast( FORMAT (StartUur,'hh')as int)-1 as varchar),':', cast(FORMAT (StartUur,'mm')as varchar)) as Time)");

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "EindUur",
                table: "Diensten",
                nullable : false,
                oldClrType : typeof(TimeSpan),
                oldNullable : true);
            migrationBuilder.Sql("update Diensten set EindUur = cast(concat(cast(cast( FORMAT (EindUur,'hh')as int)-1 as varchar),':', cast(FORMAT (EindUur,'mm')as varchar)) as Time)");

            migrationBuilder.AlterColumn<int>(
                name: "Uurloon",
                table: "BusChauffeurs",
                nullable : false,
                oldClrType : typeof(double));
            migrationBuilder.Sql("SELECT AantalMinutenAdministratieveTijdVoorDienst FROM Instellingen UPDATE Instellingen SET AantalMinutenAdministratieveTijdVoorDienst = AantalMinutenAdministratieveTijdVoorDienst / 60000;");
        }
    }
}