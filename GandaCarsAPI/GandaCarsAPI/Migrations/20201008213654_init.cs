﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GandaCarsAPI.Migrations {
    public partial class init : Migration {
        protected override void Up(MigrationBuilder migrationBuilder) {
            migrationBuilder.CreateTable(
                name: "BusChauffeurs",
                columns : table => new {
                    Id = table.Column<string>(nullable: false),
                        Voornaam = table.Column<string>(nullable: false),
                        Achternaam = table.Column<string>(nullable: true),
                        Uurloon = table.Column<int>(nullable: false),
                        Email = table.Column<string>(nullable: true),
                        GeboorteDatum = table.Column<DateTimeOffset>(nullable: false),
                        Stelsel = table.Column<int>(nullable: false)
                },
                constraints : table => {
                    table.PrimaryKey("PK_BusChauffeurs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Feestdagen",
                columns : table => new {
                    Id = table.Column<string>(nullable: false),
                        Dag = table.Column<DateTimeOffset>(nullable: false),
                        Naam = table.Column<string>(nullable: true)
                },
                constraints : table => {
                    table.PrimaryKey("PK_Feestdagen", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Instellingen",
                columns : table => new {
                    Id = table.Column<string>(nullable: false),
                        AantalMinutenAdministratieveTijdVoorDienst = table.Column<int>(nullable: false, defaultValue: 15),
                        FietsVergoeding = table.Column<double>(nullable: false),
                        Nachtwerk = table.Column<double>(nullable: false),
                        NachtwerkStart = table.Column<double>(nullable: false),
                        NachtwerkEinde = table.Column<double>(nullable: false),
                        NachtwerkNaStartAdministratieveTijd = table.Column<double>(nullable: false),
                        NachtwerkVoorEindeAdministratieveTijd = table.Column<double>(nullable: false),
                        OnderbrekingsVergoeding = table.Column<double>(nullable: false),
                        Premie = table.Column<double>(nullable: false),
                        Zaterdag = table.Column<double>(nullable: false),
                        AmplitudeMiliseconds = table.Column<double>(nullable: false)
                },
                constraints : table => {
                    table.PrimaryKey("PK_Instellingen", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Diensten",
                columns : table => new {
                    Id = table.Column<string>(nullable: false),
                        Naam = table.Column<string>(nullable: false),
                        StartUur = table.Column<DateTimeOffset>(nullable: false),
                        EindUur = table.Column<DateTimeOffset>(nullable: false),
                        StartDag = table.Column<int>(nullable: false),
                        EindDag = table.Column<int>(nullable: false),
                        DagVanToevoegen = table.Column<DateTimeOffset>(nullable: false),
                        BusChauffeurId1 = table.Column<string>(nullable: true),
                        BusChauffeurId = table.Column<string>(nullable: true)
                },
                constraints : table => {
                    table.PrimaryKey("PK_Diensten", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Diensten_BusChauffeurs_BusChauffeurId",
                        column : x => x.BusChauffeurId,
                        principalTable: "BusChauffeurs",
                        principalColumn: "Id",
                        onDelete : ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Diensten_BusChauffeurs_BusChauffeurId1",
                        column : x => x.BusChauffeurId1,
                        principalTable: "BusChauffeurs",
                        principalColumn: "Id",
                        onDelete : ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EffectieveDiensten",
                columns : table => new {
                    Id = table.Column<string>(nullable: false),
                        Naam = table.Column<string>(nullable: true),
                        Start = table.Column<DateTimeOffset>(nullable: false),
                        Einde = table.Column<DateTimeOffset>(nullable: false),
                        BusChauffeurId = table.Column<string>(nullable: true),
                        AndereMinuten = table.Column<int>(nullable: false),
                        DagVanToevoegen = table.Column<DateTimeOffset>(nullable: false),
                        GerelateerdeDienstId = table.Column<string>(nullable: true)
                },
                constraints : table => {
                    table.PrimaryKey("PK_EffectieveDiensten", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EffectieveDiensten_BusChauffeurs_BusChauffeurId",
                        column : x => x.BusChauffeurId,
                        principalTable: "BusChauffeurs",
                        principalColumn: "Id",
                        onDelete : ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EffectieveDiensten_EffectieveDiensten_GerelateerdeDienstId",
                        column : x => x.GerelateerdeDienstId,
                        principalTable: "EffectieveDiensten",
                        principalColumn: "Id",
                        onDelete : ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Onderbrekingen",
                columns : table => new {
                    Id = table.Column<string>(nullable: false),
                        StartUur = table.Column<DateTimeOffset>(nullable: false),
                        EindUur = table.Column<DateTimeOffset>(nullable: false),
                        StartDag = table.Column<int>(nullable: false),
                        EindDag = table.Column<int>(nullable: false),
                        type = table.Column<int>(nullable: false),
                        DienstId = table.Column<string>(nullable: true)
                },
                constraints : table => {
                    table.PrimaryKey("PK_Onderbrekingen", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Onderbrekingen_Diensten_DienstId",
                        column : x => x.DienstId,
                        principalTable: "Diensten",
                        principalColumn: "Id",
                        onDelete : ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EffectieveOnderbrekingen",
                columns : table => new {
                    Id = table.Column<string>(nullable: false),
                        EffectieveStart = table.Column<DateTimeOffset>(nullable: false),
                        EffectiefEinde = table.Column<DateTimeOffset>(nullable: false),
                        type = table.Column<int>(nullable: false),
                        EffectieveDienstId = table.Column<string>(nullable: true)
                },
                constraints : table => {
                    table.PrimaryKey("PK_EffectieveOnderbrekingen", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EffectieveOnderbrekingen_EffectieveDiensten_EffectieveDienstId",
                        column : x => x.EffectieveDienstId,
                        principalTable: "EffectieveDiensten",
                        principalColumn: "Id",
                        onDelete : ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Diensten_BusChauffeurId",
                table: "Diensten",
                column: "BusChauffeurId");

            migrationBuilder.CreateIndex(
                name: "IX_Diensten_BusChauffeurId1",
                table: "Diensten",
                column: "BusChauffeurId1");

            migrationBuilder.CreateIndex(
                name: "IX_EffectieveDiensten_BusChauffeurId",
                table: "EffectieveDiensten",
                column: "BusChauffeurId");

            migrationBuilder.CreateIndex(
                name: "IX_EffectieveDiensten_GerelateerdeDienstId",
                table: "EffectieveDiensten",
                column: "GerelateerdeDienstId",
                unique : true,
                filter: "[GerelateerdeDienstId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_EffectieveOnderbrekingen_EffectieveDienstId",
                table: "EffectieveOnderbrekingen",
                column: "EffectieveDienstId");

            migrationBuilder.CreateIndex(
                name: "IX_Onderbrekingen_DienstId",
                table: "Onderbrekingen",
                column: "DienstId");
        }

        protected override void Down(MigrationBuilder migrationBuilder) {
            migrationBuilder.DropTable(
                name: "EffectieveOnderbrekingen");

            migrationBuilder.DropTable(
                name: "Feestdagen");

            migrationBuilder.DropTable(
                name: "Instellingen");

            migrationBuilder.DropTable(
                name: "Onderbrekingen");

            migrationBuilder.DropTable(
                name: "EffectieveDiensten");

            migrationBuilder.DropTable(
                name: "Diensten");

            migrationBuilder.DropTable(
                name: "BusChauffeurs");
        }
    }
}