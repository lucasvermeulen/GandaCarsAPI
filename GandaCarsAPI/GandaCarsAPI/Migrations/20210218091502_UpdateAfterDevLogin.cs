﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GandaCarsAPI.Migrations
{
    public partial class UpdateAfterDevLogin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "Diensten");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "Diensten",
                nullable: false,
                defaultValue: 0);
        }
    }
}
