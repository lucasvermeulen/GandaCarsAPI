﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GandaCarsAPI.Migrations
{
    public partial class deleteBehavior : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Diensten_BusChauffeurs_BusChauffeurId",
                table: "Diensten");

            migrationBuilder.DropForeignKey(
                name: "FK_EffectieveDiensten_BusChauffeurs_BusChauffeurId",
                table: "EffectieveDiensten");

            migrationBuilder.DropForeignKey(
                name: "FK_LoonficheSettings_BusChauffeurs_BusChauffeurId",
                table: "LoonficheSettings");

            migrationBuilder.AddColumn<string>(
                name: "BusChauffeurId1",
                table: "LoonficheSettings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BusChauffeurId1",
                table: "EffectieveDiensten",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LoonficheSettings_BusChauffeurId1",
                table: "LoonficheSettings",
                column: "BusChauffeurId1");

            migrationBuilder.CreateIndex(
                name: "IX_EffectieveDiensten_BusChauffeurId1",
                table: "EffectieveDiensten",
                column: "BusChauffeurId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Diensten_BusChauffeurs_BusChauffeurId",
                table: "Diensten",
                column: "BusChauffeurId",
                principalTable: "BusChauffeurs",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_EffectieveDiensten_BusChauffeurs_BusChauffeurId",
                table: "EffectieveDiensten",
                column: "BusChauffeurId",
                principalTable: "BusChauffeurs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EffectieveDiensten_BusChauffeurs_BusChauffeurId1",
                table: "EffectieveDiensten",
                column: "BusChauffeurId1",
                principalTable: "BusChauffeurs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_LoonficheSettings_BusChauffeurs_BusChauffeurId",
                table: "LoonficheSettings",
                column: "BusChauffeurId",
                principalTable: "BusChauffeurs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_LoonficheSettings_BusChauffeurs_BusChauffeurId1",
                table: "LoonficheSettings",
                column: "BusChauffeurId1",
                principalTable: "BusChauffeurs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Diensten_BusChauffeurs_BusChauffeurId",
                table: "Diensten");

            migrationBuilder.DropForeignKey(
                name: "FK_EffectieveDiensten_BusChauffeurs_BusChauffeurId",
                table: "EffectieveDiensten");

            migrationBuilder.DropForeignKey(
                name: "FK_EffectieveDiensten_BusChauffeurs_BusChauffeurId1",
                table: "EffectieveDiensten");

            migrationBuilder.DropForeignKey(
                name: "FK_LoonficheSettings_BusChauffeurs_BusChauffeurId",
                table: "LoonficheSettings");

            migrationBuilder.DropForeignKey(
                name: "FK_LoonficheSettings_BusChauffeurs_BusChauffeurId1",
                table: "LoonficheSettings");

            migrationBuilder.DropIndex(
                name: "IX_LoonficheSettings_BusChauffeurId1",
                table: "LoonficheSettings");

            migrationBuilder.DropIndex(
                name: "IX_EffectieveDiensten_BusChauffeurId1",
                table: "EffectieveDiensten");

            migrationBuilder.DropColumn(
                name: "BusChauffeurId1",
                table: "LoonficheSettings");

            migrationBuilder.DropColumn(
                name: "BusChauffeurId1",
                table: "EffectieveDiensten");

            migrationBuilder.AddForeignKey(
                name: "FK_Diensten_BusChauffeurs_BusChauffeurId",
                table: "Diensten",
                column: "BusChauffeurId",
                principalTable: "BusChauffeurs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EffectieveDiensten_BusChauffeurs_BusChauffeurId",
                table: "EffectieveDiensten",
                column: "BusChauffeurId",
                principalTable: "BusChauffeurs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_LoonficheSettings_BusChauffeurs_BusChauffeurId",
                table: "LoonficheSettings",
                column: "BusChauffeurId",
                principalTable: "BusChauffeurs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
