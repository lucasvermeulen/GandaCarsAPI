﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GandaCarsAPI.Migrations
{
    public partial class rmNieuweOverUrenBc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NieuweOverUrenBc",
                table: "LoonficheSettings");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "NieuweOverUrenBc",
                table: "LoonficheSettings",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
